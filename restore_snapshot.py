import datetime
import re
import time
from traceback import format_exc

import click
import elasticsearch
# try:
#     from .config import (
#         SNAPSHOT_ENDPOINT,
#         SNAPSHOT_PATTERNS,
#         SNAPSHOT_REPOSITORY,
#     )
# except ModuleNotFoundError:
#     from config import (
#         SNAPSHOT_ENDPOINT,
#         SNAPSHOT_PATTERNS,
#         SNAPSHOT_REPOSITORY,
#     )

SNAPSHOT_PATTERNS = {
    'mdr_og-*': [],
}
SNAPSHOT_REPOSITORY = "s3_repository"
SNAPSHOT_ENDPOINT = "http://elb-es-staging.int.everest-qa.aws.mintel.com:9200"
ELASTICSEARCH = elasticsearch.Elasticsearch(SNAPSHOT_ENDPOINT, timeout=60)


@click.command()
@click.option('--prefix', default=None, help='String to prepend to newly created index names.')
@click.option('--interactive/--non-interactive',
              default=True,
              help='Prompts with available snapshots to restore.'
                   ' If non-interactive mode set, restores the latest snapshot')
@click.option('--keep-old', is_flag=True, help='Whether old indices should be kept')
def restore(prefix, interactive, keep_old):
    # Validate arguments
    if prefix == '':
        raise click.BadParameter('If prefix option used, must specify a prefix.')

    # Figure out which snapshot to restore
    snapshots = ELASTICSEARCH.cat.snapshots(
        repository=SNAPSHOT_REPOSITORY,
        h='id,end_epoch',
        s='end_epoch',
        format='json'
    )
    if interactive:
        click.echo('Available snapshots:\n')
        click.echo("|  id   |   timestamp   |")
        for snapshot in snapshots:
            click.echo("|  {}  |  {}  |".format(
                snapshot['id'], format_epoch(snapshot['end_epoch'])
            ))
        snapshot_to_restore = click.prompt(
            "Type the id of the snapshot to restore",
            prompt_suffix=":\n"
        )
        if snapshot_to_restore not in [ss['id'] for ss in snapshots]:
            raise click.ClickException('Invalid snapshot supplied. Aborting.')

    else:
        snapshot_to_restore = snapshots[-1]['id']
        click.echo('Will restore:\nid: {0}\ntimestamp: {1}'.format(
            snapshot_to_restore, format_epoch(snapshots[-1]['end_epoch']))
        )

    statuses = {}
    for pattern, aliases in SNAPSHOT_PATTERNS.items():
        try:
            complete = restore_pattern(
                snapshot_to_restore,
                pattern,
                aliases,
                prefix,
                interactive,
                keep_old,
            )
            statuses[pattern] = 'Complete' if complete else 'Aborted'
        except Exception:
            statuses[pattern] = 'Error: {}'.format(format_exc())

    click.echo('\n=====Restore complete for all patterns=====')
    for pattern in sorted(statuses.keys()):
        click.echo('{}: {}'.format(pattern, statuses[pattern]))


def restore_pattern(
    snapshot,
    index_pattern,
    aliases,
    prefix=None,
    interactive=False,
    keep_old=False,
):
    """
    Restores indices from a given snapshot matching an index pattern and assigns\
    appropriate aliases.
    :param str snapshot: Snapshot id
    :param str index_pattern: Pattern of indices to restore
    :param list(str) aliases: Aliases to apply to restore indices
    :param str prefix: Optional string to prepend to restored indices
    :param bool interactive: Whether to prompt for user input
    :param bool keep_old: Whether to preserve replaced indices
    :return: Whether pattern was restored
    :rtype: bool
    """
    click.echo('\n=====Restoring index pattern {}====='.format(index_pattern))
    if interactive and not click.confirm(
        "Proceed to restore index pattern '{}'?".format(index_pattern)
    ):
        return False

    indices_to_restore = get_indices_from_snapshot(snapshot, index_pattern)
    if not indices_to_restore:
        click.echo("No '{0}' indices in snapshot. Skipping pattern.".format(index_pattern))
        return False

    additional_kwargs = {}
    if prefix:
        # Configure rename for incoming indices from snapshot
        additional_kwargs['rename_pattern'] = '(.+)'
        additional_kwargs['rename_replacement'] = '{}$1'.format(prefix)
    else:
        resolve_clashes(index_pattern, indices_to_restore, interactive)

    # Start restore
    click.echo("Beginning restore...")
    ELASTICSEARCH.cluster.put_settings({
        'transient': {'indices.recovery.max_bytes_per_sec': '1000mb'}
    })
    ELASTICSEARCH.snapshot.restore(
        repository=SNAPSHOT_REPOSITORY,
        snapshot=snapshot,
        body={
            "indices": ','.join(sorted(indices_to_restore)),
            "ignore_unavailable": False,
            "include_aliases": False,
            **additional_kwargs
        },
        master_timeout="60m",
        wait_for_completion=False,
    )

    poll_for_restore_success()

    for alias in aliases:
        swap_aliases(index_pattern, alias, indices_to_restore, keep_old)

    # Reset data recovery rate to configuration setting
    ELASTICSEARCH.cluster.put_settings({
        'transient': {'indices.recovery.max_bytes_per_sec': None}
    })
    return True


def resolve_clashes(index_pattern, indices_to_restore, interactive=False):
    """
    Deletes any indices from the cluster whose name is also in the snapshot
    :param str index_pattern:
    :param set(str) indices_to_restore: Set of indices being restored
    :param bool interactive: Whether to ask for input or just delete
    :rtype: None
    """
    all_existing_indices = get_index_names_by_pattern(index_pattern)
    clashes = all_existing_indices.intersection(indices_to_restore)
    if clashes:
        if interactive:
            click.echo('Some clashing index names found in both snapshot and cluster:')
            click.echo('\n'.join(sorted(clashes)))
            click.confirm('These will be deleted, which may result in some downtime. Proceed?', abort=True)

        ELASTICSEARCH.indices.delete(
            index=','.join(sorted(clashes)), ignore_unavailable=True,
        )


def poll_for_restore_success():
    restore_start_time = datetime.datetime.now()
    while True:
        time.sleep(10)
        cluster_status = ELASTICSEARCH.cluster.health()
        if cluster_status['status'] == 'green':
            break
        click.echo("Waiting for completion.")
    restore_end_time = datetime.datetime.now()
    time_taken = (restore_end_time - restore_start_time).total_seconds()
    click.echo("Restore complete! Took {}s".format(time_taken))


def get_index_names_by_pattern(pattern):
    """
    :param str pattern: Pattern of indices to return
    :rtype: set(str)
    """
    return {
        i['index'] for i in
        ELASTICSEARCH.cat.indices(index=pattern, h='index', format='json')
    }

def get_index_names_by_alias(alias):
    """
    :param str alias: Alias of indices to return
    :rtype: set(str)
    """
    return {
        i['index'] for i in
        ELASTICSEARCH.cat.aliases(name=alias, h='index', format='json')
    }


def swap_aliases(index_pattern, alias, restored_indices, keep_old=False):
    """
    Removes alias from all current indices and adds it to indices matching the pattern
    :param str index_pattern: Pattern of index name to receive alias
    :param str alias:
    :param set(str) restored_indices: Set of index names that were restored
    :param bool keep_old: Whether to keep existing indices matching the alias
    :rtype: None
    """
    actions = []
    indices_to_alias = get_index_names_by_pattern(index_pattern)
    indices_to_alias.intersection_update(restored_indices)
    if indices_to_alias:
        actions.append({
            'add': {'indices': sorted(indices_to_alias), 'alias': alias}
        })

    existing_aliased_indices = sorted(get_index_names_by_alias(alias))
    if existing_aliased_indices:
        actions.append({
            'remove': {
                'indices': existing_aliased_indices,
                'alias': alias
            },
        })
    if actions:
        ELASTICSEARCH.indices.update_aliases(body={'actions': actions})
        click.echo("Swapped indices under alias '{}'.".format(alias))

    if existing_aliased_indices and not keep_old:
        ELASTICSEARCH.indices.delete(
            index=','.join(existing_aliased_indices),
            ignore_unavailable=True,
        )
        click.echo("Deleted old indices under alias '{}'.".format(alias))


def format_epoch(epoch):
    """
    :param str epoch:
    :return: Human readable date string
    """
    return datetime.datetime.utcfromtimestamp(int(epoch)).strftime('%c')


def get_indices_from_snapshot(snapshot, index_pattern):
    """
    :param str snapshot: Id of snapshot
    :param str index_pattern:
    :return: Set of index names from snapshot matching provided pattern
    """
    resp = ELASTICSEARCH.snapshot.get(
        repository=SNAPSHOT_REPOSITORY,
        snapshot=snapshot,
        verbose=False,
    )
    return {
        idx for idx
        in resp['snapshots'][0]['indices']
        if re.search(index_pattern.replace('*', '.+'), idx)
    }


if __name__ == '__main__':
    restore()
