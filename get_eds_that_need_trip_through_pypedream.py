
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
from datetime import datetime
from collections import defaultdict
from utils import grouper
import dateutil.parser


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
QA_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"
PROD_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"
ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    qa_analytics={
        'hosts': [QA_ES_ANALYTICS],
        'sniff_on_start': True
    },
    prod_analytics={
        'hosts': [PROD_ES_ANALYTICS],
        'sniff_on_start': True
    },
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

search = Search(
    using='staging', index="eds_og"
).filter(Q("bool", must=[
    Q("exists", field="media.image.url"),
    Q("match", notUsableReason__raw="Missing image url")
])).source([])

print(search.count())

with open('fixed_eds_to_pypedream.csv', 'w', newline='') as pypefile:
    pwriter = csv.writer(pypefile, delimiter=',')
    for og in search.scan():
        pwriter.writerow([og.meta.id])

