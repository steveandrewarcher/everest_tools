from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    }
)

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

missing_og_ids = []
expected_count = 0
with open('pfb-2019-07-29T00:00:00Z-2019-07-29T23:59:59Z-obs_postIds_present_in_ogs.csv', newline='') as missing_ogs_file:
    missing_og_reader = csv.reader(missing_ogs_file, delimiter=',')
    for row in missing_og_reader:
        expected_count += 1
        missing_og_ids.append(row[1])

found_count = 0
for chunk in grouper(missing_og_ids, 1000):
    staging_srch = Search(
        using='staging', index="pfb_og"
    ).query(Q('ids', values=list(chunk)))
    found_count += staging_srch.count()

print(expected_count)
print(found_count)
