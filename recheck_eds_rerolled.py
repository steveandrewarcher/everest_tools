import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

# with open("pfb_ogids_with_mismatched_endDate.csv", newline='') as mismatch_file:
#     og_ids_to_check = [row[0] for row in csv.reader(mismatch_file, delimiter=",")]

with open("eds_ogs_in_staging_but_not_analytics.txt", newline='') as mismatch_file:
    og_ids_to_check = [row[0] for row in csv.reader(mismatch_file, delimiter=",")]

a_og_search = Search(
    using="analytics", index=f"eve",
).filter(Q("bool", must=[
    Q("ids", values=og_ids_to_check)
])).source([])

print(a_og_search.count())
found_in_analytics = [og.meta.id for og in a_og_search.scan()]


s_og_search = Search(
    using="staging", index=f"eds_og",
).filter(Q("bool", must=[
    Q("ids", values=og_ids_to_check),
    ~Q("match", notUsableReason="Rolled Up Into Another OG")
])).source(["sent", "email"])

print(s_og_search.count())
found_in_staging = {og.meta.id: (og.email, og.sent) for og in s_og_search.scan()}

mismatch = set(found_in_staging.keys()) - set(found_in_analytics)


with open("eds_still_weird_in_staging.csv", "w", newline="") as still_bad_file:
    still_bad_writer = csv.writer(still_bad_file, delimiter=",")
    for og_id in mismatch:
        still_bad_writer.writerow([og_id])

with open("eds_pairs_still_weird_in_staging.csv", "w", newline="") as still_bad_pair_file:
    still_bad_pair_writer = csv.writer(still_bad_pair_file, delimiter=",")
    for og_id in mismatch:
        still_bad_pair_writer.writerow(list(found_in_staging[og_id]))

