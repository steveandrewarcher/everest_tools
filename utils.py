from itertools import islice

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk