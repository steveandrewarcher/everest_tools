from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

def time_windows(start_date, end_date, day_step=7):
    start_window = maya.when(start_date, timezone="UTC")
    end_window = start_window.add(days=day_step - 1).snap("@d+23h+59m+59s")
    while end_window < maya.when(end_date):
        yield {
            "start": start_window.iso8601(),
            "end": end_window.iso8601()
        }
        start_window = start_window.add(days=day_step)
        end_window = end_window.add(days=day_step)
    yield {
        "start": start_window.iso8601(),
        "end": maya.when(end_date).snap("@d+23h+59m+59s").iso8601()
    }


def week_windows(start_date, end_date):
    for week in time_windows(start_date, end_date, 7):
        yield week

def find_missing_ogs(date):
    staging_srch = Search(
        using='staging', index="pat_og"
    ).query(Q("bool", must=[
        Q("range", endDate={"gte": date["start"], "lte": date["end"]}),
        Q("match", isUsable=True),
    ]))
    pp('searching staging')
    ogs_in_window = []
    ogs_info_in_window = {}
    for og in staging_srch.scan():
        ogs_in_window.append(og.meta.id)
        ogs_info_in_window[og.meta.id] = {
            "id": og.meta.id,
            "creativeId": og.PAT.creativeId,
            "device": og.PAT.device,
            "ingestionTs": og.ingestionTs
        }
    staging_ogs_count = len(ogs_in_window)
    pp(f'found {staging_ogs_count}')
    missing_og_info = []
    for chunk in grouper(ogs_in_window, 10000):
        pp(f'searching chunk in analytics')
        chunk_count = len(chunk)
        pp(f"length of this staging chunk: {chunk_count}")
        analytics_srch = Search(
            using='analytics', index=f'eve'
        ).query(Q("ids", values=list(chunk)))
        analytics_count = analytics_srch.count()
        pp(f".count() of this analytics chunk: {analytics_count}")

        if chunk_count != analytics_count:
            analytics_chunk = []
            analytics_chunk_info = {}
            for og in analytics_srch.scan():
                analytics_chunk.append(og.meta.id)
            y = len(analytics_chunk)
            pp(f"length of analytics chunk built with .scan(): {y}")
            missing_og_ids = list(set(chunk) - set(analytics_chunk))
            missing_og_info.extend([ogs_info_in_window[og_id] for og_id in missing_og_ids])

    missing_ogs_count = len(missing_og_info)
    pp(f"found {missing_ogs_count} missing from analytics")

    return missing_og_info

today = datetime.date.today().strftime("%m_$d_Y")
filename = f"missing_ogs_{today}.csv"
with open("test_fix.csv", "w", newline='') as csvfile:
    og_writer = csv.writer(csvfile, delimiter=',')
    for week in time_windows('2019-05-06', '2019-06-18'):
        start_date = week["start"]
        end_date = week["end"]
        pp(f"processing week {start_date} to {end_date}")
        missing_ogs = find_missing_ogs(week)
        count = len(missing_ogs)
        pp(f"would push {count}")
        for og in missing_ogs:
            og_writer.writerow([
                og["id"],
                og["creativeId"],
                og["device"],
                og["ingestionTs"],
                f"week {start_date} to {end_date}"
            ])