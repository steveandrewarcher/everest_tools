import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)
unholy_partners = []
with open("dem_dupe_groups.csv", newline='') as dupesfile:
    for row in csv.reader(dupesfile, delimiter=','):
        for thing in row:
            unholy_partners.append(thing)
print(len(unholy_partners))

with open("eds_media_found_in_20191001_snapshot.csv", newline='') as mediafile:
    found_guys = [row[0] for row in csv.reader(mediafile, delimiter=',')]

print(len(found_guys))

not_in_unholy_partners = set(found_guys) - set(unholy_partners)

print(len(not_in_unholy_partners))

print(list(not_in_unholy_partners)[:10])