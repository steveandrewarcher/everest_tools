import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

a_search = Search(
    using="analytics", index="eve"
).filter(Q("bool", must=[
    Q("match", vendor="EDS"),
    Q("range", endDate={"gte": "2018-10-01T00:00:00", "lte": "2018-10-01T23:23:59"}),
    Q("exists", field="isPublishedOps")
]))

print(a_search.count())