import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

with open("all_eds_ogs_missing_media.csv", newline='') as nomedia_file:
    nomedia_reader = csv.reader(nomedia_file, delimiter=",")
    ogs_with_no_media = [row[0] for row in nomedia_reader]


with open("eds_ogs_WITH_media_in_staging.csv", "w", newline='') as withmedia_file:
    withmedia_writer = csv.writer(withmedia_file, delimiter=",")
    counter = 0
    for chunk in grouper(ogs_with_no_media, 100):
        counter += 100
        if counter % 1000 == 0:
            print(counter)
        staging_search = Search(
            using="staging", index="eds_og"
        ).filter(Q("bool", must=[
            Q("exists", field="media"),
            Q("ids", values=list(chunk))
        ])).source(["media"])
        count = staging_search.count()
        if count:
            print(f"{count} found in staging")
            for og in staging_search.scan():
                withmedia_writer.writerow([og.meta.id, json.dumps(og.media.to_dict())])