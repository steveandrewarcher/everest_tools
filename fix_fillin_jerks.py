import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
QA_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"
PROD_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"
ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    qa_analytics={
        'hosts': [QA_ES_ANALYTICS],
        'sniff_on_start': True
    },
    prod_analytics={
        'hosts': [PROD_ES_ANALYTICS],
        'sniff_on_start': True
    },
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

p_srch = Search(
    using="prod_analytics", index="eve-201905"
).filter(Q("bool", must=[
    Q("exists", field="media"),
    Q("match", vendor="EDS"),
    Q("match", og_o_join="og"),
    Q("range", endDate={"gte": "2018-01-01", "lte": "2019-09-30"})
])).source([])

print(p_srch.count())

ogs_in_prod = [og.meta.id for og in p_srch.scan()]

q_srch = Search(
    using="qa_analytics", index="restoreeve-201905"
).filter(Q("bool", must=[
    Q("exists", field="media"),
    Q("match", vendor="EDS"),
    Q("match", og_o_join="og"),
    Q("range", endDate={"gte": "2018-01-01", "lte": "2019-09-30"}),
])).source([])

print(q_srch.count())

ogs_in_snap = [og.meta.id for og in q_srch.scan()]

not_in_prod = set(ogs_in_snap) - set(ogs_in_prod)

print(len(not_in_prod))

with open("eds_in_snapshot_but_not_in_prod.csv", "w", newline='') as notfile:
    notwriter = csv.writer(notfile, delimiter='')
    for og in not_in_prod:
        notwriter.writerow()