from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from aws_sso import boto3_client


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

staging_srch = Search(
        using='staging', index="pat_o"
    ).query(Q("bool", must=[
        Q("match", PAT__creativeType="Uncaptured"),
        Q("match", isUsable=True),
    ])).source(["batchId"])

filenames = set()
for o in staging_srch.scan():
    filenames.add("".join(o., 1)[1:]))

print(len(filenames))

records = []
for filename in filenames:
    records.append({
        "Data": json.dumps(
            {
                "source":[filename],
                "run_id":"notanemptystring"
            }
        ),
        "PartitionKey": filename,
    })

records1 = records[:300]
records2 = records[300:]

kinesis_client = boto3_client('kinesis', region_name='us-east-2')
kinesis_client.describe_stream(StreamName="kin-ba-pat-file")

output = kinesis_client.put_records(
    StreamName="kin-ba-pat-file",
    Records=records1,
)

print(output)

output = kinesis_client.put_records(
    StreamName="kin-ba-pat-file",
    Records=records2,
)

print(output)