from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from dateutil.parser import parse
from collections import defaultdict
from utils import grouper

ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

with open("eds_with_email_address.csv", "w", newline="") as emailfile:
    with open("eds_with_whitespace.csv", "w", newline="") as whitefile:
        emailwriter = csv.writer(emailfile, delimiter=',')
        whitewriter = csv.writer(whitefile, delimiter=',')
        search = Search(
            using="staging", index='eds_og-201911'
        ).source(["displayTitle", "isUsable", "isPublishedOps", "marketingCompany"])
        print(search.count())
        counter = 0
        for og in search.scan():
            counter += 1
            if counter % 1000 == 0:
                print(counter)
            ogdict = og.to_dict()
            if "displayTitle" in ogdict:
                if og.displayTitle[-1:] == " ":
                    whitewriter.writerow([og.meta.id, og.displayTitle, ogdict.get("isUsable"), ogdict.get("isPublishedOps"), json.dumps(ogdict.get("marketingCompany", {}))])
                if "@" in og.displayTitle:
                    emailwriter.writerow([og.meta.id, og.displayTitle, ogdict.get("isUsable"), ogdict.get("isPublishedOps"), json.dumps(ogdict.get("marketingCompany", {}))])