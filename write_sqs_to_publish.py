from aws_sso import boto3_client, boto3_resource
import json
import csv
import time

queue_name = 'publish-expert-input-queue'
sqs = boto3_resource('sqs')
queue = sqs.get_queue_by_name(QueueName=queue_name)
delete_count = 0

messages_to_requeue = []
count = 0
with open('publish_messages_to_requeue_minus_dupes.csv', newline='') as message_file:
    message_reader = csv.reader(message_file, delimiter=",")
    for message in message_reader:
        messages_to_requeue.append({
            'Id': message[0],
            'MessageBody': message[1],
        })
        if len(messages_to_requeue) == 10:
            requeue_response = queue.send_messages(
                Entries=messages_to_requeue
            )
            count += 10
            if count % 1000 == 0:
                print(count)
            messages_to_requeue = []

print("done")

