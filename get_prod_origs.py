from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime

PROD = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    prod={
        'hosts': [PROD],
        'sniff_on_start': True
    }
)

srch = Search(
    using="prod", index="eds_orig_v2"
).source(["timebased"])

# counter = 0
# with open("originators.csv", "w", newline='') as csvfile:
#     orig_writer = csv.writer(csvfile, delimiter=',')
for orig in srch.scan():
    if len(orig.timebased) > 1:
        print(orig.meta.id)
    # orig_writer.writerow([
    #     orig.meta.id,
    #     orig.timebased[-1].country
    # ])

