from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

index_suffix = "202001"

s_search = Search(
    using='staging', index=f"eds_og"
).filter(Q("bool", must=[
    Q("exists", field="isPublishedOps"),
    ~Q("match", notUsableReason="Rolled Up Into Another OG")
])).source(["email", "sent"])

print(s_search.count())

counter = 0
s_ogs = {}
for og in s_search.scan():
    counter += 1
    if counter % 1000 == 0:
        print(counter)
    s_ogs[og.meta.id] = (og.email, og.sent)

a_search = Search(
    using="analytics", index=f"eve"
).filter(Q("bool", must=[
    Q("match", og_o_join="og"),
    Q("match", vendor="EDS"),
    Q("exists", field="isPublishedOps"),
    ~Q("match", notUsableReason="Rolled Up Into Another OG")
])).source([])

print(a_search.count())

counter = 0
a_ogs = []
for og in a_search.scan():
    counter += 1
    if counter % 1000 == 0:
        print(counter)
    a_ogs.append(og.meta.id)

for og in a_ogs:
    s_ogs.pop(og, None)

pairs = set(s_ogs.values())

ogs = list(s_ogs.keys())

with open(f"eds_pairs_in_staging_but_not_analytics.txt", "w", newline="") as missingpairfile:
    missingpairwriter = csv.writer(missingpairfile)
    for pair in pairs:
        missingpairwriter.writerow(list(pair))

with open(f"eds_ogs_in_staging_but_not_analytics.txt", "w", newline="") as missingfile:
    missingwriter = csv.writer(missingfile)
    for og in ogs:
        missingwriter.writerow([og])