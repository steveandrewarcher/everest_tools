import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)

ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

with open("eds_media_found_in_20191001_snapshot.csv", newline='') as snapshot_file:
    ogs_in_snapshot = [row[0] for row in csv.reader(snapshot_file, delimiter=",")]


with open("eds_ogs_with_body_text_in_assets_but_no_media.csv", newline='') as assets_file:
    ogs_in_assets = [row[0] for row in csv.reader(assets_file, delimiter=",")]

difference =  set(ogs_in_snapshot) - set(ogs_in_assets)

print(len(difference))
