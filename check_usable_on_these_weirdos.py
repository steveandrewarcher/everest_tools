from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from dateutil.parser import parse
from collections import defaultdict
from utils import grouper

ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

with open("eds_still_weird_in_staging.csv", newline='') as weirdos_file:
    weirdo_ids = [row[0] for row in csv.reader(weirdos_file, delimiter=",")]

srch = Search(
    using="staging", index="eds_og"
).filter(Q("bool", must=[
    Q("ids", values=weirdo_ids),
    ~Q("match", isUsable=False),
    ~Q("range", endDate={"gte": "2019-10-25", "lte": "2019-10-26T23:59:59"})
]))

print(srch.count())
for og in srch.scan():
    print(og.meta.id)