from aws_sso import boto3_client
import csv

s3 = boto3_client('s3', region_name='us-east-2')

keys_in_bucket = []
start_after = None
while True:
    ks = {
        "Bucket": 'everest-s3-prod-ue2-min-dgs-incoming',
    }
    if start_after:
        ks["StartAfter"] = start_after

    response = s3.list_objects_v2(**ks)
    print(response.keys())
    try:
        keys_in_bucket.extend([obj['Key'] for obj in response['Contents']])
        if len(response['Contents']) < 1000:
            break
        start_after = response['Contents'][-1]["Key"]
    except:
        break

with open("keys_in_dgs_bucket_prod.csv", "w", newline="") as dgsfile:
    w = csv.writer(dgsfile, delimiter=",")
    for key in keys_in_bucket:
        w.writerow([key])