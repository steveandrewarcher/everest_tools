import csv

with open("all_pat_analytics_ogs_202001.csv", newline="") as a_file:
    a_reader = csv.reader(a_file, delimiter=",")
    a_map = set(row[0] for row in a_reader)

with open("all_pat_staging_ogs_202001.csv", newline="") as s_file:
    s_reader = csv.reader(s_file, delimiter=",")
    s_map = set(row[0] for row in s_reader)


not_in_analytics = s_map - a_map

with open("pat_ogs_missing_from_a_202001", "w", newline="") as mismatch_file:
    mismatch_writer = csv.writer(mismatch_file, delimiter=',')
    for ogid in not_in_analytics:
        mismatch_writer.writerow([ogid])