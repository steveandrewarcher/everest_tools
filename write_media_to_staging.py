from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

ES_STAGING = "readonly.elb-es-staging.int.everest-qa.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"


connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

c = connections.get_connection('staging')

print(c)

with open("dem_medias.csv", newline='') as medias_file:
    media_reader = csv.reader(medias_file, delimiter=',')

    for chunk in grouper(media_reader, 1000):
        operations = [
            {
                '_op_type': 'update',
                '_index': 'eds_og',
                '_type': 'document',
                '_id': row[0],
                'doc': json.loads(row[1])
            } for row in chunk
        ]

    

        bulk(connection.get_connection('staging'))