import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

# with open("pfb_ogids_with_mismatched_endDate.csv", newline='') as mismatch_file:
#     og_ids_to_check = [row[0] for row in csv.reader(mismatch_file, delimiter=",")]

with open("pat_still_not_in_analytics.csv", newline='') as mismatch_file:
    og_ids_to_check = [row[0] for row in csv.reader(mismatch_file, delimiter=",")]

a_og_search = Search(
    using="analytics", index=f"eve",
).filter(Q("bool", must=[
    Q("ids", values=og_ids_to_check)
])).source(["endDate"])

a_map = {og.meta.id:og.endDate for og in a_og_search.scan()}

s_og_search = Search(
    using="staging", index=f"pat_og",
).filter(
    Q("ids", values=og_ids_to_check)
).source(["endDate", "notUsableReason"])

for s in s_og_search.scan():
    if not s.notUsableReason in ["Vendor industry not set", "Vendor industry not usable"]:
        print(s.meta.id)

# weirdos = []
# with open("some_real_serious_pat_jerks.csv", "w", newline="") as jerks:
#     jerks_writer = csv.writer(jerks, delimiter=",")
#     for og in s_og_search.scan():
#         try:
#             if og.endDate != a_map.get(og.meta.id):
#                 weirdos.append(og.meta.id)
#         except:
#             jerks_writer.writerow([og.meta.id])


# with open("pat_still_not_in_analytics.csv", "w", newline="") as still_bad_file:
#     still_bad_writer = csv.writer(still_bad_file, delimiter=",")
#     for og_id in weirdos:
#         still_bad_writer.writerow([og_id])
