from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

missing_ogs = []
with open('missing_ogs.csv', newline='') as missing_ogs_file:
    missing_og_reader = csv.reader(missing_ogs_file, delimiter=',')
    for row in missing_og_reader:
        missing_ogs.append(row[0])

analytics_srch = Search(
    using='analytics', index=f'eve'
).query(Q("ids", values=list(missing_ogs)))

found = []
for og in analytics_srch.scan():
    found.append(og.meta.id)

still_missing = list(set(missing_ogs) - set(found))

pp(still_missing)