from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q

ES_STAGING = "elb-es-staging.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    }
)

mutant_search = Search(
    using='staging', index="eds_og"
).filter(Q("bool", must=[
    Q("exists", field="EDS.bodyText"),
    Q("exists", field="EDS.bodyTextUrl"),
    Q("exists", field="EDS.wholeEmailUrl"),
    ~Q("exists", field="vendor"),
]))

count = mutant_search.count()
print(count)

mutant_search.delete()
