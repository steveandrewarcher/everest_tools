from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
import json

ES_STAGING = "elb-es-staging.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    }
)

mutant_search = Search(
    using='staging', index="eds_og"
).filter(Q("bool", must=[
    Q("exists", field="EDS.bodyText"),
    Q("exists", field="EDS.bodyTextUrl"),
    Q("exists", field="EDS.wholeEmailUrl"),
    ~Q("exists", field="vendor"),
]))

count = mutant_search.count()
print(count)

counter = 0
misplaced_values = {}

for og in mutant_search.scan():
    if counter % 100 == 0:
        print(counter, count)
    counter += 1
    misplaced_values[og.meta.id] = {
        "bodyText": og.EDS.bodyText,
        "bodyTextUrl": og.EDS.bodyTextUrl,
        "wholeEmailUrl": og.EDS.wholeEmailUrl
    }
with open("misplaced_email_values.json", "w") as misplaced_file:
    json.dump(misplaced_values, misplaced_file)
