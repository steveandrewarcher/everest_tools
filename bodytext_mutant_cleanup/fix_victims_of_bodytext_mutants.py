from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
import json
import csv

ES_STAGING = "elb-es-staging.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    }
)

with open("misplaced_email_values.json") as misplaced_file:
    misplaced_values = json.load(misplaced_file)

og_search = Search(
    using="staging", index="eds_og"
).filter("ids", values=list(misplaced_values.keys()))

print(og_search.count())

counter = 0
operations = []
for og in og_search.scan():
    if counter % 100 == 0:
        print(counter)
    counter += 1
    doc = {
        "EDS": {
            "bodyText": misplaced_values[og.meta.id]["bodyText"],
            "bodyTextUrl": misplaced_values[og.meta.id]["bodyTextUrl"],
            "wholeEmailUrl": misplaced_values[og.meta.id]["wholeEmailUrl"]
        }
    }
    try:
        email = og.EDS["email"]
        sent = og.EDS["sent"]
    except:
        print(og.meta.id)
        continue
    routing = f"{email}:{sent}"
    operations.append({
        '_op_type': 'update',
        '_index': og.meta.index,
        '_type': 'doc',
        '_id': og.meta.id,
        '_routing': routing,
        'doc': doc
    })


result = bulk(connections.get_connection('staging'), operations, raise_on_error=False, request_timeout=60)

with open("eds_mutants_missing_normies.csv", "a+", newline='') as missingfile:
    missing_writer = csv.writer(missingfile, delimiter=',')
    for thing in result[1]:
        if thing["update"]["status"] == 404 and thing["update"]["error"]["type"] == "document_missing_exception":
            missing_writer.writerow([thing["update"]["_id"]])
        else:
            print(thing)