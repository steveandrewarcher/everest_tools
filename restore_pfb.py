from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime

ES_ANALYTICS = "elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    analytics={
        "hosts": [ES_ANALYTICS],
        "sniff_on_start": True
    }
)

bad_og_search = Search(
    using="analytics", index="eve"
).query(
    Q("bool", must=[
        Q("match", channelType="PFB"),
        Q("match", og_o_join="og"),
        Q("range", endDate={"gte": "2018-01-01"}),
        Q("match", vendor="PAT"),
        ~Q("exists", field="scratch"),
        ~Q("match", isUsable=False),
    ])
)

print(bad_og_search.count())

with open("last_naughty_pfb_ogs.csv", "w", newline="") as csvfile:
    naughty_writer = csv.writer(csvfile, delimiter=",")
    for og in bad_og_search.scan():
        naughty_writer.writerow([og.meta.id])
