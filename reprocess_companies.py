import hashlib
import json
from aws_sso import boto3_client
import requests
import csv
from pprint import pprint as pp

# source_companies = []
# merged_to = {}
# with open('reprocess_companies.csv') as csv_file:
#     reader = csv.reader(csv_file, delimiter=",")
#     for row in reader:
#         source_companies.append((row[0],row[1]))
#         merged_to[f'query{row[0]}'] = row[3]


# pp(merged_to['query1957'])

# query_template = """
#     {query_id}: companies(id: "{company_uri}") {{
#         edges {{
#             node {{
#                 mergedTo {{
#                     id
#                 }}
#             }}
#         }}
#     }}
# """

# querystring = "{"
# for company_id, uri in source_companies:
#     querystring += query_template.format(query_id="query"+company_id, company_uri=uri)
# querystring += "}"
# query = {"query": querystring.replace("\n","")}

# tax_url = "https://taxonomy-api.int.everest-prod.aws.mintel.com/graphql"

# response = requests.post(tax_url, json=query)
# results = json.loads(response.content)["data"]

# pp(results["query1957"])

# def build_record(query_key, merge_target):
#     source_id = query_key.replace('query', '')
#     data = json.dumps({ 'path': 'MERGE', 'kwargs': {'source_uri': f'taxonomy-v1/company/{source_id}', 'target_uri': merge_target} }, sort_keys=True)
#     return {"Data": data, "PartitionKey": hashlib.sha1(data.encode('utf-8')).hexdigest()}

# records = []
# for key, data in results.items():
#     merge_target = data['edges'][0]['node']['mergedTo']['id']
#     if merged_to[key] != merge_target:
#         pp('failed!')
#         pp(key)
#         pp(data)
#     else:
#         records.append(build_record(key, merge_target))


def build_record():
    data = json.dumps({ 'path': 'REPROCESS_URI', 'kwargs': {"uri": "taxonomy-v1/company/1194"} }, sort_keys=True)
    return {"Data": data, "PartitionKey": hashlib.sha1(data.encode('utf-8')).hexdigest()}

client = boto3_client('kinesis', region_name='us-east-2')
stream_name = 'kin-st-sas-shepherd'
client.describe_stream(StreamName=stream_name)
client.put_records(StreamName=stream_name, Records=[build_record()])
