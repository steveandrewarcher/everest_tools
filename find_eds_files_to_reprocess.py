from aws_sso import boto3_client, boto3_resource
import json
from datetime import datetime, timedelta
import csv

s3_client = boto3_client('s3', region_name='us-east-2')

def make_days(start, end):
    start_date = datetime.strptime(start, '%Y%m%d')
    end_date = datetime.strptime(end, '%Y%m%d')
    while start_date <= end_date:
        yield start_date.strftime('%Y%m%d')
        start_date += timedelta(days=1)

with open("eds_files_to_reprocess_dayone.csv", "w", newline='') as csv_of_json:
    json_writer = csv.writer(csv_of_json, delimiter=',')
    for day in make_days('20180101', '20180101'):
        s3 = boto3_resource("s3")
        s3_bucket = s3.Bucket("everest-s3-shared-ue2-eds-incoming")
        for obj in s3_bucket.objects.filter(
            Prefix=f"v4.2/email/{day}"
        ):
            json_writer.writerow([f"s3://everest-s3-shared-ue2-eds-incoming/{obj.key}"])
