import csv

with open("pfb_to_reprocess.csv", newline='') as sourcefile:
    og_reader = csv.reader(sourcefile, delimiter=',')
    ogs = []
    for og in og_reader:
        ogs.append(og[0])


with open("pfb_to_reprocess1.csv", "w", newline='') as file1:
    writer1 = csv.writer(file1, delimiter=',')
    for og in ogs[:5000000]:
        writer1.writerow([og])

with open("pfb_to_reprocess2.csv", "w", newline='') as file2:
    writer2 = csv.writer(file2, delimiter=',')
    for og in ogs[5000000:10000000]:
        writer2.writerow([og])

with open("pfb_to_reprocess3.csv", "w", newline='') as file3:
    writer3 = csv.writer(file3, delimiter=',')
    for og in ogs[10000000:]:
        writer3.writerow([og])