from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
import os
import requests


ids = []
for entry in os.scandir('missing_pat'):
    with open(os.fsdecode(entry.path), newline='') as missing_ogs_file:
        missing_og_reader = csv.reader(missing_ogs_file, delimiter=',')
        missing_og_info = []
        missing_og_reader.__next__()
        for row in missing_og_reader:
            ids.append(row[0])

ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
)

srch = Search(
    using='analytics', index="eve"
).query("ids", values=ids).source(["PAT"])

urls = []
for og in srch.scan():
    urls.append(og.PAT["assetUrl"])

for url in urls:
    r = requests.get(url)
    if r.status_code != 403:
        print(r.status_code)