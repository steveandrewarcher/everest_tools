from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

a_og_search = Search(
    using="analytics", index=f"eve",
).filter(Q("bool", must=[
    Q("match", vendor="PAT"),
    ~Q("match", channelType="PFB"),
    Q("match", og_o_join="og")
])).source([])

print(a_og_search.count())
a_ogs = []
counter = 0
for og in a_og_search.scan():
    counter += 1
    if counter % 10000 == 0:
        print(counter)
    try:
        a_ogs.append(og.meta.id)
    except:
        print(og.meta.id, " is being naughty")

print(len(a_ogs))

with open(f"all_pat_analytics_ogs_202001.csv", "w", newline="") as a_ogs_file:
    a_writer = csv.writer(a_ogs_file, delimiter=",")
    for og in a_ogs:
        a_writer.writerow([og])