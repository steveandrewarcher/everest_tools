
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
from datetime import datetime
from collections import defaultdict
from utils import grouper
import dateutil.parser


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
QA_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"
PROD_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"
ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    qa_analytics={
        'hosts': [QA_ES_ANALYTICS],
        'sniff_on_start': True
    },
    prod_analytics={
        'hosts': [PROD_ES_ANALYTICS],
        'sniff_on_start': True
    },
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

asearch = Search(
    using="prod_analytics", index="eve-201810"
).filter(Q("bool", must=[
    Q("term", vendor="PAT"),
    ~Q("term", channelType="PFB"),
    Q("term", og_o_join="o")
])).source(["ingestionTs"])
print("doing analytics")
print(asearch.count())
counter = 0
a_map={}
for og in asearch.params(size=1000).scan():
    counter += 1
    if counter % 10000 == 0:
        print(counter, "a")
    a_map[og.meta.id] = dateutil.parser.isoparse(og.ingestionTs)

try:
    with open("analytics_ogs_and_ingestionTs.csv", 'w', newline='') as cachefile:
        cachewriter = csv.writer(cachefile, delimiter=',')
        for og, ts in a_map.items():
            cachewriter.writerow([
                og,
                ts
            ])
except:
    pass

# with open("analytics_ogs_and_ingestionTs.csv", newline='') as cachefile:
#     cachereader = csv.reader(cachefile, delimiter=',')
#     a_map = {row[0]: dateutil.parser.isoparse(row[1]) for row in cachereader}

with open("obs_updated_in_staging_after_analytics.csv", "a+", newline='') as badfile:
    bad_writer = csv.writer(badfile, delimiter=',')
    counter = 0
    files = set()
    for chunk in grouper(a_map.keys(), 10000):
        ssearch = Search(
            using="staging", index="pat_o"
        ).filter(Q("bool", must=[
            Q("term", isUsable=True),
            Q('ids', values=list(chunk))
        ])).source(["ingestionTs", "batchId"])
        for og in ssearch.params(size=10000).scan():
            counter += 1
            if counter % 10000 == 0:
                print(counter, "s")
            if not a_map.get(og.meta.id) or (dateutil.parser.isoparse(og.ingestionTs) - a_map[og.meta.id]).total_seconds() > (3600 * 12):
                bad_writer.writerow([og.meta.id, og.batchId])
                files.add(og.batchId)

with open("pat_files_with_out_of_date_obs.csv", "a+",  newline='') as filefile:
    file_writer = csv.writer(filefile, delimiter=',')
    for filename in files:
        file_writer.writerow([
            filename[4:]
        ])
