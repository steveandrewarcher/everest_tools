from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)


s_og_search = Search(
    using="staging", index=f"pat_og-202001",
).filter(
    Q("exists", field="isPublishedOps")).source([])
print(s_og_search.count())
s_ogs = []
counter = 0
for og in s_og_search.scan():
    counter += 1
    if counter % 10000 == 0:
        print(counter)
    try:
        s_ogs.append(og.meta.id)
    except:
        print(og.meta.id, " is being naughty")

print(len(s_ogs))

with open(f"all_pat_staging_ogs_202001.csv", "w", newline="") as s_ogs_file:
    s_writer = csv.writer(s_ogs_file, delimiter=",")
    for og in s_ogs:
        s_writer.writerow([og])




