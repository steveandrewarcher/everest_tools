import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

QA_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    },
    qa_analytics={
        'hosts': [QA_ANALYTICS],
        'sniff_on_start': True
    }
)

search = Search(
    using="analytics", index="eve"
).filter(Q("bool", must=[
    Q("term", vendor="EDS"),
    Q("term", og_o_join="og"),
    Q("exists", field="media"),
    ~Q("exists", field="isPublishedOps"),
    Q("range", ingestionTs={"lte": "2020-02-01"})
])).source([])
print(search.count())

# with open("unpublished_eds_with_media.csv", "w", newline='') as unfile:
#     unwriter = csv.writer(unfile, delimiter=',')
#     for og in search.scan():
#         unwriter.writerow([og.meta.id])


with open("unpublished_eds_with_media.csv", newline='') as readfile:
    ogs = [row[0] for row in csv.reader(readfile, delimiter=',')]
    with open("unpublished_eds_with_media6.csv", "w", newline='') as unfile:
        unwriter = csv.writer(unfile, delimiter=',')
        for og in ogs[100000:]:
            unwriter.writerow([og])