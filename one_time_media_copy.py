from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

og_search = Search(
    using='analytics', index="eve-201901"
).filter(Q("bool", must=[
    Q("term", vendor="EDS"),
    Q("exists", field="media"),
])).source(["media"])

count = og_search.count()

medias = {}
counter = 0
for og in og_search.scan():
    if counter % 1000 == 0:
        print(counter, count)
    counter += 1
    medias[og.meta.id] = og.media

operations = [
    {
        '_op_type': 'update',
        '_index': 'eds_og-201901',
        '_type': 'doc',
        '_id': id,
        'doc': {
            "media": media
        }
    } for id, media in medias.items()
]

print(operations[0])


result = bulk(connections.get_connection('staging'), operations[:50000])
print(result)
print("***")
result = bulk(connections.get_connection('staging'), operations[50000:100000])
print(result)
print("***")
result = bulk(connections.get_connection('staging'), operations[100000:])
print(result)