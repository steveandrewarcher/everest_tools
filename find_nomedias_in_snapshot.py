import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    qa_analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

with open("all_eds_ogs_missing_media.csv", newline='') as nomedia_file1:
    ogs_with_no_media = [row[0] for row in csv.reader(nomedia_file1, delimiter=",")]


with open("eds_ogs_with_no_media_found_in_20191001_snapshot.csv", "w", newline='') as media_file:
    media_writer = csv.writer(media_file, delimiter=",")
    counter = 0
    for chunk in grouper(ogs_with_no_media, 100):
        counter += 100
        if counter % 1000 == 0:
            print(counter)
        media_search = Search(
            using="qa_analytics", index="restoreeve-*"
        ).filter(Q("bool", must=[
            Q("ids", values=list(chunk)),
            Q("exists", field="media")
        ])).source(['media'])
        count = media_search.count()
        if count:
            print(f"{count} found in snapshot")
            for og in media_search.scan():
                media_writer.writerow([og.meta.id, json.dumps(og.media.to_dict())])