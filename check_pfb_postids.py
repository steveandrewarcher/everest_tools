from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from dateutil.parser import parse
from collections import defaultdict

ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)


def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

def check_for_mismatches_in_chunk(chunk):
    og_ids = [og.meta.id for og in chunk]
    # obs_count_on_ogs = sum([og.observationIdsCount for og in chunk])
    new_obs_search = Search(
        using='staging', index="pfb_o"
    ).query(
        Q("terms", obsGroupId__keyword=og_ids) & Q("match", isUsable=True)
    ).source(include=["obsGroupId", "ingestionTs"])

    # actual_obs_count = obs_search.count()
    return True, new_obs_search # actual_obs_count != obs_count_on_ogs, obs_search

obs_search = Search(
    using='staging', index="pfb_o"
).query(Q("match", isUsable=True) & Q("match", batchId__keyword="PAT:s3://everest-s3-prod-ue2-pat-pfb-incoming/PM_MINTEL_SOCIAL_20190718_US.csv")
).source(include=["obsGroupId"])

print("today's og ids")
print(obs_search.count())

og_id_list=set()
for obs in obs_search.scan():
    og_id_list.add(obs.obsGroupId)
print('scanned to find og ids')
og_id_list = list(og_id_list)
# og_query = Q("match", isUsable=True) & Q(
#     "range", ingestionTs={"gte": window["start"], "lte": window["end"]}
# )
# og_id_list=[]
# with open('ogs_in_the_three_files.csv', newline='') as missing_ogs_file:
#     og_reader = csv.reader(missing_ogs_file, delimiter=',')
#     for row in og_reader:
#         og_id_list.append(row[0])
# print("read the file")
# print(len(og_id_list))
og_search = Search(
    using='staging', index="pfb_og"
).query(
    Q("ids", values=og_id_list)
).source(include=["observationIdsCount", "ingestionTs"])

ogs_in_window = []
for og in og_search.scan():
    ogs_in_window.append(og)
print('scanned the ogs')
print(len(ogs_in_window))
ogs_with_mismatched_obs_count = []
chunk_count = 0
for chunk in grouper(ogs_in_window, 1000):
    chunk_count += 1
    print(f"big chunk {chunk_count}")
    if check_for_mismatches_in_chunk(chunk)[0]:
        print("big chunk bad, chop again")
        # chop it up again
        for smaller_chunk in grouper(chunk, 100):
            print("small chunk")
            mismatches_in_chunk, new_obs_search = check_for_mismatches_in_chunk(smaller_chunk)
            if mismatches_in_chunk:
                print('small chunk bad, do slow things')
                #do slow stuff
                count_map = defaultdict(lambda: 0)
                timestamp_map = defaultdict(lambda: datetime.datetime.min.strftime("%Y-%m-%d"))
                obses = []
                print(new_obs_search.to_dict())
                import pdb; pdb.set_trace()
                for o in new_obs_search.scan():
                    obses.append(o)
                pdb.set_trace()
                for obs in obses:
                    count_map[obs.obsGroupId] = count_map[obs.obsGroupId] + 1
                    if parse(obs.ingestionTs) > parse(timestamp_map[obs.obsGroupId]):
                        timestamp_map[obs.obsGroupId] = obs.ingestionTs
                pdb.set_trace()
                for og in smaller_chunk:
                    if count_map[og.meta.id] != og.observationIdsCount:
                        ogs_with_mismatched_obs_count.append(og.meta.id)
                    elif timestamp_map[og.meta.id] > og.ingestionTs:
                        ogs_with_mismatched_obs_count.append(og.meta.id)
                print(len(ogs_with_mismatched_obs_count))

print("final result")
print(len(ogs_with_mismatched_obs_count))

if len(ogs_with_mismatched_obs_count):
    with open("naughty_ogs.csv", "w", newline='') as csvfile:
        og_writer = csv.writer(csvfile, delimiter=',')
        for og in ogs_with_mismatched_obs_count:
            og_writer.writerow([
                og
            ])

# staging_srch = Search(
#     using='staging', index="pfb_o"
# ).query(Q("bool", must=[
#     Q("range", obsDate={"gte": "2019-06-01", "lte": "2019-06-01"}),
#     Q("term", isUsable=True)
# ]))

# o_count = staging_srch.count()

# set_of_posts = set()
# for o in staging_srch.scan():
#     set_of_posts.add(o.postId)

# og_srch = Search(
#     using='staging', index="pfb_og"
# ).query(Q("bool", must=[
#     Q("range", ={"gte": "2019-06-01", "lte": "2019-06-01"}),
#     Q("term", isUsable=True)
# ]))

# set_of_ogs_posts = set()
# for og in og_srch.scan():
#     set_of_ogs_posts.add(og.PAT.postId)