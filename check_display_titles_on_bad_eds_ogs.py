import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

# with open("all_eds_ogs_missing_media.csv", newline='') as nomedia_file1:
#     og_reader = csv.reader(nomedia_file1, delimiter=",")
#     pairs = set([(row[1],row[2]) for row in og_reader])

# og_count = len(ogs_with_no_media)
# with open("sender_date_pairs_missing_media.csv", "a+", newline='') as pairs_file:
#     pairs_writer = csv.writer(pairs_file, delimiter=",")
#     print(f"searching {og_count} ogs for their pairs")
#     counter = 0
#     for chunk in grouper(ogs_with_no_media[150000:], 500):
#         counter += 500
#         if counter % 1000 == 0:
#             print(counter)
#         pair_search = Search(
#             using="staging", index="eds_og"
#         ).filter(
#             Q("ids", values=list(chunk))
#         ).source(["email","sent"])
#         for og in pair_search.scan():
#             pairs_writer.writerow([og.email, og.sent])


# with open("all_eds_pairs_in_staging_missing_media.csv", newline='') as pairs_file:
#     pairs_reader = csv.reader(pairs_file, delimiter=",")
#     pairs = set([(row[0],row[1]) for row in pairs_reader])

# with open("all_eds_pairs_in_staging_missing_media.csv", "w", newline='') as pairs_file:
#     pairs_writer = csv.writer(pairs_file, delimiter=',')
#     for pair in pairs:
#         pairs_writer.writerow([
#             pair[0],
#             pair[1]
#         ])

# with open("all_eds_pairs_in_staging_missing_media.csv", newline='') as pairs_file:
#     pairs_reader = csv.reader(pairs_file, delimiter=",")
#     pairs = [(row[0],row[1]) for row in pairs_reader]

# with open("all_ogs_from_pairs_in_staging_with_missing_media.csv", "w", newline='') as ogs_file:
#     ogs_writer = csv.writer(ogs_file, delimiter=",")
#     pairs_count = len(pairs)
#     print(f"searching {pairs_count} pairs")
#     counter = 0
#     for chunk in grouper(pairs, 500):
#         counter += 500
#         if counter % 1000 == 0:
#             print(counter)
#         musts = [
#             Q("bool", must=[
#                 Q("term", email=pair[0]),
#                 Q("term", sent=pair[1])
#             ]) for pair in chunk
#         ]
#         og_search = Search(
#             using="staging", index="eds_og"
#         ).filter(
#             Q("bool", should=musts)
#         ).source(["observationIds", "sent", "email"])
#         for og in og_search.params(size=10000).scan():
#             obs_ids = tuple(sorted(list(og.observationIds)))
#             pair = (og.email, og.sent)
#             ogs_writer.writerow([json.dumps(pair), og.meta.id, json.dumps(obs_ids)])

pairs_and_ogs = defaultdict(dict)
with open("all_ogs_from_pairs_in_staging_with_missing_media.csv", newline='') as ogs_file:
    og_reader = csv.reader(ogs_file, delimiter=",")
    counter = 0
    for row in og_reader:
        counter +=1
        if counter % 10000 == 0:
            print(counter)
        pair = tuple(json.loads(row[0]))
        pairs_and_ogs[pair].update({
            row[1]: tuple(json.loads(row[2]))
        })

weirdo_count = 0
with open("dem_dupe_groups_in_staging.csv", "w", newline='') as dupe_file:
    dupe_writer = csv.writer(dupe_file, delimiter=",")
    print("finding dupes")
    for pair, og in pairs_and_ogs.items():
        dupe_map = defaultdict(set)
        for ogid, observations in og.items():
            dupe_map[observations].add((ogid, pair))
        for og_set in dupe_map.values():
            if len(og_set) > 1:
                weirdo_count += len(og_set)
                dupe_writer.writerow([json.dumps([thing[0], [foo for foo in thing[1]]]) for thing in og_set])

print(f"{weirdo_count} weirdos")