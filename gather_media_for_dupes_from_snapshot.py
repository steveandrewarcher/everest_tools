import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

QA_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    },
    qa_analytics={
        'hosts': [QA_ANALYTICS],
        'sniff_on_start': True
    }
)


with open("both_dupes_have_media_in_staging.csv", "a+", newline='') as bothfile:
    both_writer = csv.writer(bothfile, delimiter=',')
    with open("dem_dupe_groups_in_staging.csv", newline='') as dupefile:
        dupe_reader = csv.reader(dupefile, delimiter=",")
        media_map = defaultdict(dict)
        counter = 0
        for row in dupe_reader:
            counter += 1
            if counter % 10 == 0:
                print(counter)
            ogs_plus_pairs = [(json.loads(og_and_pair)[0], tuple(json.loads(og_and_pair)[1])) for og_and_pair in row]

            ogs = [og_plus_pair[0] for og_plus_pair in ogs_plus_pairs]
            snapshot_search = Search(
                using="qa_analytics", index="restoreeve-*"
            ).filter(Q('bool', must=[
                Q("ids", values=ogs),
                Q("exists", field="media")
            ]))
            found_media = [og.media for og in snapshot_search.scan()]
            if len(found_media) == 1:
                for og_plus_pair in ogs_plus_pairs:
                    media_map[og_plus_pair] = found_media[0].to_dict()

with open("media_from_snapshot_for_dupes_in_staging.csv", "w", newline='') as media_file:
    media_writer = csv.writer(media_file, delimiter=',')
    for og_plus_pair, media in media_map.items():
        media_writer.writerow([
            json.dumps(og_plus_pair),
            json.dumps(media)
        ])
