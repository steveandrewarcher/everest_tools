from aws_sso import boto3_client, boto3_resource
import json
from datetime import datetime, timedelta
import csv

s3_client = boto3_client('s3', region_name='us-east-2')
s3 = boto3_resource("s3")
s3_bucket = s3.Bucket("everest-s3-shared-ue2-eds-incoming")


buckets = s3.buckets.all()
for b in buckets:
    print(b)

with open("eds_fillin_files.csv", "w", newline='') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',')
    for obj in s3_bucket.objects.filter(
        Prefix=f"v4.2/fix_20191024/email/"
    ):
        parts_list = obj.key.split("/")
        parts_list.remove("fix_20191024")
        new_key = "/".join(parts_list)
        filewriter.writerow([f"s3://everest-s3-shared-ue2-eds-incoming/{new_key}"])
        print(obj.key, new_key)
        # s3.Object("everest-s3-shared-ue2-eds-incoming", new_key).copy_from(CopySource = obj.key)
        s3_client.copy_object(
            CopySource={'Bucket': "everest-s3-shared-ue2-eds-incoming", 'Key': obj.key},
            Bucket="everest-s3-shared-ue2-eds-incoming",
            Key=new_key,
        )

