from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-qa.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

og_search = Search(
        using='staging', index="min_og"
    ).query(Q("ids", values=["MIN:DRM/20190919-0111091"]))

print(og_search.count())
for og in og_search.scan():
    print('hi')
    print(og.meta.index)
    print(og.vendor)