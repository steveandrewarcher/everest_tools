from aws_sso import boto3_client, boto3_resource
import json
from datetime import datetime, timedelta
import csv

s3_client = boto3_client('s3', region_name='us-east-2')
s3 = boto3_resource("s3")
s3_bucket = s3.Bucket("everest-s3-shared-ue2-eds-incoming")


with open("old_files_that_showed_up_on_20200109.txt", "a+", newline='') as old_file:
    old_writer = csv.writer(old_file)
    counter = 0
    for obj in s3_bucket.objects.filter(
        Prefix=f"v4.2/email/201912"
    ):
        counter += 1
        lastmodstring = obj.last_modified.strftime("%Y%m%d")
        filedatestring = obj.key.split("/")[2]
        if counter % 100 == 0:
            print(counter)
            print(lastmodstring, filedatestring)
        # if lastmodstring == "20200109" and lastmodstring != filedatestring:
        #     old_writer.writerow([obj.key])
