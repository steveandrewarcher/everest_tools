from aws_sso import boto3_client
from itertools import islice
import json
import csv
from pprint import pprint as pp
import os

PYPEDREAM_KINESIS_STREAM = "kin-st-sas-operations-pipeline"
kinesis_client = boto3_client('kinesis', region_name='us-east-2')
kinesis_client.describe_stream(StreamName=PYPEDREAM_KINESIS_STREAM)

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

def push_to_pypedream(og_ids):
    """
    Push og ids to pypdream
    """
    def _to_record(obsGroupId):
        return {
            "Data": json.dumps(
                {'obsGroupId': obsGroupId, 'reprocess': True, 'vendor': 'PAT'}
            ),
            "PartitionKey": obsGroupId,
        }
    count = len(og_ids)
    pp(f"pushing {count} records")
    for batch in grouper(og_ids, 500):
        records = [_to_record(_) for _ in batch]
        output = kinesis_client.put_records(
            StreamName=PYPEDREAM_KINESIS_STREAM,
            Records=records,
        )

with open('one_weird_eds_og.csv', newline='') as ogs_file:
    og_reader = csv.reader(ogs_file, delimiter=',')
    og_ids = []
    for row in og_reader:
        og_ids.append(row[0])

    push_to_pypedream(og_ids)
