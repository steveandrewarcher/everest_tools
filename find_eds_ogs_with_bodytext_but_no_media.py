import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

with open("all_eds_ogs_missing_media.csv", newline='') as nomedia_file1:
    ogs_with_no_media = [row[0] for row in csv.reader(nomedia_file1, delimiter=",")]

ogs_found = set()
with open("eds_ogs_with_anything_in_assets_but_no_media.csv", "w", newline='') as nomedia_file:
    nomedia_writer = csv.writer(nomedia_file, delimiter=",")
    counter = 0
    for chunk in grouper(ogs_with_no_media, 100):
        counter += 100
        if counter % 1000 == 0:
            print(counter)
        asset_search = Search(
            using="assets", index="assets-*"
        ).filter(Q("bool", must=[
            Q("match", vendor="EDS"),
            Q("terms", parentId=list(chunk))
        ])).source(["parentId"])
        count = asset_search.count()
        if count:
            print(f"{count} found in assets")
            for asset in asset_search.scan():
                ogs_found.add(asset.parentId)

    for og in ogs_found:
        nomedia_writer.writerow([og])