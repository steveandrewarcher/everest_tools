from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

s_search = Search(
    using='staging', index="pat_og"
).filter(Q("bool", must=[
    Q("match", isPublishedOps=True),
])).source([])

print(s_search.count())

counter = 0
s_ogs = []
for og in s_search.scan():
    counter += 1
    if counter % 1000 == 0:
        print(counter)
    s_ogs.append(og.meta.id)

a_search = Search(
    using="analytics", index="eve"
).filter(Q("bool", must=[
    Q("match", og_o_join="og"),
    Q("match", vendor="PAT"),
    Q("match", isPublishedOps=True),
]))

print(a_search.count())

counter = 0
a_ogs = []
for og in a_search.scan():
    counter += 1
    if counter % 1000 == 0:
        print(counter)
    a_ogs.append(og.meta.id)

missing_ogs = set(s_ogs) - set(a_ogs)

with open("pat_in_staging_but_not_analytics.txt", "w", newline="") as missingfile:
    missingwriter = csv.writer(missingfile)
    for og in missing_ogs:
        missingwriter.writerow([og])