import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from everest_elasticsearch_dsl.documents.staging.mtv import TelevisionGroup
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
from datetime import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-qa.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

x = TelevisionGroup(startDate=datetime(2018,1,1))

x.save()