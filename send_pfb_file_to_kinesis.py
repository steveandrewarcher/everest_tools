from aws_sso import boto3_client
import json

kinesis_client = boto3_client('kinesis', region_name='us-east-2')
kinesis_client.describe_stream(StreamName="kin-st-min-dgs-filehandler")

record = {
    "Data": json.dumps(
        {
            "source":["s3://everest-s3-qa-ue2-min-dgs-incoming/2019030MISSING_ROW8213938_MINT0001-28-1.zip"],
        }
    ),
    "PartitionKey": "s3://everest-s3-qa-ue2-min-dgs-incoming/2019030MISSING_ROW8213938_MINT0001-28-1.zip"
}

output = kinesis_client.put_records(
    StreamName="kin-st-min-dgs-filehandler",
    Records=[record],
)

print(output)