import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

search = Search(
    using="staging", index="pat_og"
).filter(Q("bool", must=[
    Q("range", endDate={"gte": "2018-01-01"}),
    ~Q("exists", field="scratch"),
    ~Q("match", isUsable=False),
    Q("match", recordType="og")
])).source(["email", "sent"])

print(search.count())

with open("pat_ogs_unprocessed.csv", "w", newline='') as missingfile:
    missingwriter = csv.writer(missingfile, delimiter=',')

    for og in search.scan():
        missingwriter.writerow([og.meta.id])
