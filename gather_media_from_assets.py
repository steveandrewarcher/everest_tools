import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

def build_media(asset):
    if asset.mediaType == "image":
        if "url" in asset:
            return {
                asset.mediaType: {
                    "vendorUrl": asset.vendorUrl,
                    "fileExtensionMismatch": asset.fileExtensionMismatch,
                    "fileSize": asset.fileSize,
                    "width": asset.width,
                    "mimetype": asset.mimetype,
                    "createdInEnv": asset.createdInEnv,
                    "resolution": asset.resolution,
                    "url": asset.url,
                    "height": asset.height
                }
            }
    if asset.mediaType == "edsImage":
        if "emailIds" in asset:
            return {
                asset.mediaType: {
                    "emailIds": list(asset.emailIds),
                    "imageId": asset.imageId,
                    "url": asset.url
                }
            }
        else:
            return {
                asset.mediaType: {
                    "emailIds": list(asset.email_ids),
                    "imageId": asset.image_id,
                    "url": asset.url
                }
            }
    if asset.mediaType == "fullText-eds-body-text":
        return {
            asset.mediaType: {
                "mimetype": asset.mimetype,
                "encoding": asset.encoding,
                "createdInEnv": asset.createdInEnv,
                "content": asset.content,
                "url": asset.url
            }
        }

with open("all_eds_ogs_in_staging_missing_media.csv", newline='') as nomedia_file1:
    ogs_with_no_media = [list(row) for row in csv.reader(nomedia_file1, delimiter=",")]
print(len(ogs_with_no_media))
media_map = defaultdict(dict)
counter = 0
for chunk in grouper(ogs_with_no_media, 500):
    counter += 500
    print(counter)
    pair_map = {thing[0]: [thing[1], thing[2]] for thing in chunk}
    assets_search = Search(
        using="assets", index="assets-*"
    ).filter(
        "terms", parentId=list(pair_map.keys())
    )
    for asset in assets_search.scan():
        try:
            key = tuple([asset.parentId, pair_map[asset.parentId][0], pair_map[asset.parentId][1]])
            media_map[key].update(build_media(asset))
        except:
            print(key)
            print(asset.to_dict())

with open("media_from_assets_for_all_eds_ogs_in_staging_missing_media.csv", "w", newline='') as media_file:
    media_writer = csv.writer(media_file, delimiter=',')
    for key, media in media_map.items():
        media_writer.writerow([
            key[0],
            key[1],
            key[2],
            json.dumps(media)
        ])
