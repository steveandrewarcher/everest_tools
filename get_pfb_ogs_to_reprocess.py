from copy import copy
from datetime import datetime
from typing import Sequence, TextIO
from elasticsearch_dsl.connections import connections
import click
from aws_sso import boto3_client
from elasticsearch import Elasticsearch
from elasticsearch_dsl import A
from everest_elasticsearch_dsl.documents.staging.pfb import PaidFacebookObservation
from mintel_logging import LogLevel, get_logger
from dateutil.rrule import DAILY, rrule
import csv


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True,
        'timeout': 600
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

first_way_ids = set()
aggs_way_ids = set()

for day in rrule(DAILY, dtstart=datetime.strptime("2018-01-01", "%Y-%m-%d"), until=datetime.strptime("2019-11-30", "%Y-%m-%d")):
    print(day)
    search = PaidFacebookObservation.search().using("staging")
    search = search.filter(
        "match",
        obsDate=day,
    ).filter("match", isUsable=True).extra(size=0).source(["obsGroupId"])

    og_count_search = copy(search)
    og_count_search.aggs.metric(
        "og_count", A("cardinality", field="obsGroupId.keyword")
    )
    result = og_count_search.execute()
    og_count = result.aggregations.og_count.value

    ogid_search = copy(search)
    ogid_search.aggs.metric(
        "og_ids", A("terms", field="obsGroupId.keyword", size=og_count)
    )
    result = ogid_search.execute()
    ogids = [b.key for b in result.aggregations.og_ids.buckets]
    aggs_way_ids.update(ogids)

with open("pfb_to_reprocess.csv", "w", newline='') as csvfile:
    og_writer = csv.writer(csvfile, delimiter=',')
    for og in aggs_way_ids:
        og_writer.writerow([og])
    
print(len(aggs_way_ids))