import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
QA_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"
PROD_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"
ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    qa_analytics={
        'hosts': [QA_ES_ANALYTICS],
        'sniff_on_start': True
    },
    prod_analytics={
        'hosts': [PROD_ES_ANALYTICS],
        'sniff_on_start': True
    },
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

p_srch = Search(
    using="prod_analytics", index="eve-201809"
).filter(Q("bool", must=[
    Q("exists", field="media"),
    Q("term", vendor="EDS"),
    Q("term", og_o_join="og"),
    #Q("range", endDate={"gte": "2018-01-01", "lte": "2019-09-30"})
])).source([])

print(p_srch.count())

ogs_in_prod = set()
counter = 0
for og in p_srch.scan():
    counter += 1
    if counter % 100 == 0:
        print(counter)
    ogs_in_prod.add(og.meta.id)

q_srch = Search(
    using="qa_analytics", index="restoreeve-201809"
).filter(Q("bool", must=[
    Q("exists", field="media"),
    Q("term", vendor="EDS"),
    Q("term", og_o_join="og"),
])).source([])


print(q_srch.count())
ogs_in_snap = set()
counter = 0
for og in q_srch.scan():
    counter += 1
    if counter % 100 == 0:
        print(counter)
    ogs_in_snap.add(og.meta.id)


what = ogs_in_snap - ogs_in_prod

with open("still_screwy_eds_201809.csv", "w", newline='') as screwyfile:
    screwy_writer = csv.writer(screwyfile, delimiter=',')
    for og in what:
        screwy_writer.writerow([og])