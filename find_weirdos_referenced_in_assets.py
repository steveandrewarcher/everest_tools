import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)
unholy_partners = []
with open("dem_dupe_groups.csv", newline='') as dupesfile:
    for row in csv.reader(dupesfile, delimiter=','):
        for thing in row:
            unholy_partners.append(thing)
print(len(unholy_partners))


image_fields = ["vendorUrl", "fileExtensionMismatch", "fileSize","width","mimetype","createdInEnv","resolution","url","height"]

with open("weirdos_and_their_images_from_assets.csv", "w", newline='') as mediasfile:
    media_writer = csv.writer(mediasfile, delimiter=",")

    for chunk in grouper(unholy_partners, 500):
        asset_search = Search(
            using="assets", index="assets-*"
        ).filter(Q('bool', must=[
            Q("terms", parentId=list(chunk)),
            Q("term", mediaType="image")
        ])).source(image_fields + ['parentId'])

        for asset in asset_search.scan():
            image = {
                field: asset[field]
                for field in image_fields if field in asset
            }
            media_writer.writerow([
                asset.parentId,
                json.dumps(image)
            ])


