from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict

ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

with open("incomplete_pfb_ogs.csv", "w", newline='') as bad_file:
    bad_writer = csv.writer(bad_file, delimiter=',')

    og_search = Search(
        using='analytics', index="eve"
    ).filter(
        Q("bool", must=[
            Q("match", channelType="PFB"),
            Q("exists", field="media"),
            ~Q("match", isPublishedOps=True),
            Q("match", isUsable=True),
            Q("exists", field="marketingCompany")
        ])
    ).source([])
    pp(og_search.to_dict())
    print(og_search.count())
    try:
        for og in og_search.scan():
            bad_writer.writerow([og.meta.id])
    except:
        print("oops")
        for og in chunk:
            try_writer.writerow([og])