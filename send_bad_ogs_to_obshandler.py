from aws_sso import boto3_client
from itertools import islice
import json
import csv
from pprint import pprint as pp


kinesis_client = boto3_client('kinesis', region_name='us-east-2')
kinesis_client.describe_stream(StreamName="kin-ba-pat-pfb-obsgroup-handler")

records = []

def create_record(og):
    return {
        "Data": json.dumps(
            {"obsGroupId": og}
        ),
        "PartitionKey": og
    }

with open('pfb-2019-07-29T00:00:00Z-2019-07-29T23:59:59Z-obs_postIds_present_in_ogs.csv', newline='') as missing_ogs_file:
    missing_og_reader = csv.reader(missing_ogs_file, delimiter=',')
    for row in missing_og_reader:
        records.append(create_record(row[1]))

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

for chunk in grouper(records, 500):
    output = kinesis_client.put_records(
        StreamName="kin-ba-pat-pfb-obsgroup-handler",
        Records=chunk,
    )

print(output)