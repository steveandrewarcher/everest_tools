from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

FIELD_LIST = [
    "media",
    "brandedProduct",
    "brandedProductBrands",
    "brandedProductNames",
    "isGeneralBranding",
    "isPublishedOps",
    "marketingCompany",
    "scratch",
    "unbrandedProduct",
    "unbrandedProductNames",
    "isUsable",
    "notUsableReason",
]

og_search = Search(
    using='analytics', index="eve"
).filter(Q("bool", must=[
    # Q("range", startDate={"gte": "2018-07-01", "lte": "2018-09-30T23:23:59"}),
    Q("term", og_o_join="og"),
    Q("term", vendor="PAT"),
    Q('ids', values=["PAT:MUL/desktop:142473733"])
    # Q("term", channelType="PFB")
])).source(FIELD_LIST)

count = og_search.count()
print(count)
counter = 0
operations = []
for og in og_search.scan():
    if counter % 1000 == 0:
        print(counter, count)
    counter += 1

    doc = {
        FIELD: og[FIELD]
        for FIELD in FIELD_LIST if FIELD in og
    }

    # EDS ONLY - get the missing straggler fields on there
    # doc.update({
    #     "recordType": "og",
    #     "obsGroupId": og.meta.id,
    #     "channelType": "EML"
    # })

    index_suffix = og.meta.index[4:]
    if doc:
        # email = og.EDS["email"]
        # sent = og.EDS["sent"]
        routing = og.meta.id
        operations.append({
            '_op_type': 'update',
            '_index': f"pat_og-{index_suffix}",
            '_type': 'doc',
            '_id': og.meta.id,
            '_routing': routing,
            'doc': doc
        })

print(operations[0])
print(len(operations))


result = bulk(connections.get_connection('staging'), operations, raise_on_error=False)
print(result[0])
print(len(result[1]))
with open("eds_stuff_missing_from_staging_201801.csv", "a+", newline='') as missingfile:
    missing_writer = csv.writer(missingfile, delimiter=',')
    for thing in result[1]:
        if thing["update"]["status"] == 404 and thing["update"]["error"]["type"] == "document_missing_exception":
            missing_writer.writerow([thing["update"]["_id"]])
        else:
            print(thing)


