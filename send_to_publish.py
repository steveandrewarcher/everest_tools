from aws_sso import boto3_client, boto3_resource
import json
import csv

queue_name = 'publish-expert-input-queue.fifo'
sqs = boto3_resource('sqs')
queue = sqs.get_queue_by_name(QueueName=queue_name)

messages = []
with open("sqs_messages_to_requeue_to_publish.csv", newline='') as csv_file:
    message_reader = csv.reader(csv_file, delimiter=',')
    for row in message_reader:
        messages.append(json.loads(row[0]))

print(messages[0])

for message in messages:
    requeue_response = queue.send_messages(
        Entries=[message]
    )
    print(requeue_response)
