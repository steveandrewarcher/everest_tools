from aws_sso import boto3_client
import csv
from more_itertools import chunked

s3 = boto3_client('s3', region_name='us-east-2')

with open("dgs_keys_to_delete_prod.csv", newline='') as dgsfile:
    dgs_keys = [row[0] for row in csv.reader(dgsfile, delimiter=",")]


for chunk in chunked(dgs_keys, 1000):
    objects = [
        {
            "Key": key
        } for key in chunk
    ]

    response = s3.delete_objects(
        Bucket='everest-s3-prod-ue2-min-dgs-incoming',
        Delete={
            'Objects': objects
        }
    )

    print(response.get("Errors"))