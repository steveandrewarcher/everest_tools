import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

obs_search = Search(
    using="staging", index="pat_o"
).filter(Q("bool", must=[
    ~Q("exists", field="obsGroupId"),
    ~Q("term", isUsable=False)
])).source(["PAT"])
print(obs_search.count())

with open("pat_obs_without_obsGroupId.csv", "w", newline='') as obsfile:
    obs_writer = csv.writer(obsfile, delimiter=",")
    # ogs_with_no_media = []
    counter = 0
    for obs in obs_search.params(size=10000).scan():
        counter += 1
        if counter % 10000 == 0:
            print(counter)
        # ogs_with_no_media.append(og.meta.id)
        obs_writer.writerow([
            obs.meta.id,
            obs.PAT['creativeId'],
            obs.PAT['device']
        ])

with open("pat_obs_without_obsGroupId.csv", newline='') as obsfile:
    pairs = set([(row[1],row[2]) for row in csv.reader(obsfile, delimiter=',')])

print(list(pairs)[0])
with open("pat_ogs_with_obs_without_obsGroupId.csv", "w", newline='') as ogfile:
    og_writer = csv.writer(ogfile, delimiter=',')
    for pair in pairs:
        og_writer.writerow([f"PAT:MUL/{pair[1]}:{pair[0]}"])


# with open("pat_ogs_with_obs_without_obsGroupId.csv", newline='') as ogfile:
#     og_ids = [row[0] for row in csv.reader(ogfile, delimiter=',')]

# s_count = 0
# a_count = 0
# print("ogs", len(og_ids))
# counter = 0
# for chunk in grouper(og_ids, 500):
#     counter += 500
#     if counter % 1000 == 0:
#         print(counter)
#     ssearch = Search(
#         using="staging", index="pat_og"
#     ).filter(Q("ids", values=list(chunk))).source([])
#     s_count += ssearch.count()
#     if ssearch.count() < 500:
#         existing_ogs = set([og.meta.id for og in ssearch.scan()])
#         missing_ogs = set(chunk) - existing_ogs
#         print(missing_ogs)
#         break

#     asearch = Search(
#         using="analytics", index="eve"
#     ).filter(Q("ids", values=list(chunk))).source([])
#     a_count += asearch.count()

# print(f"{s_count} are in staging")
# print(f"{a_count} are in analytics")