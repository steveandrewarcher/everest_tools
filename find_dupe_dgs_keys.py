import csv
from collections import defaultdict

with open("keys_in_dgs_bucket_prod.csv", newline='') as dgsfile:
    dgs_keys = [row[0] for row in csv.reader(dgsfile, delimiter=",")]

unique_keys = set([key[15:] for key in dgs_keys])

print(len(unique_keys))

key_map = defaultdict(list)

for key in dgs_keys:
    key_map[key[15:]].append(key)

keys_to_delete = []
for suffix, key_list in key_map.items():
    if len(key_list) > 1:
        sorted_keys = sorted(key_list, key=lambda k: int(k[:14]))
        keys_to_delete.extend(sorted_keys[:-1])

print(len(keys_to_delete))

with open('dgs_keys_to_delete_prod.csv', "w", newline='') as deletefile:
    w = csv.writer(deletefile, delimiter=",")
    for key in keys_to_delete:
        w.writerow([key])
