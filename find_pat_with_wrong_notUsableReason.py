import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

search = Search(
    using="staging", index="pat_og"
).filter(Q("bool", must=[
    Q("match", notUsableReason="Missing Marketing Company"),
    Q("bool", should=[
        ~Q("exists", field="vendorIndustryName"),
        ~Q("terms", vendorIndustryName__keyword=[
            "Arts & Entertainment",
            "Auto",
            "Business",
            "Computers & Consumer Electronics",
            "Family & Parenting",
            "Financial Services",
            "Media",
            "Pharma & Healthcare",
            "Retail",
            "Style, Fashion & Beauty",
            "Telecom",
            "Travel",
        ])
    ])
]))

print(search.count())

# with open("pat_ogs_in_need_of_notUsableReason_fix.csv", "w", newline='') as ogfile:
#     ogwriter = csv.writer(ogfile, delimiter=',')

#     counter = 0
#     for og in search.scan():
#         counter += 1
#         if counter % 10000 == 0:
#             print(counter)
#         ogwriter.writerow([og.meta.id])

