from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
import csv


ES_STAGING = "elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    }
)

og_search = Search(
    using='staging', index="eds_og"
).filter(Q("bool", must=[
    ~Q("exists", field="EDS.imageUrl"),
    ~Q("exists", field="acquiredMedia.mainAsset.assetId"),
    Q("range", endDate={"gte": "2020-12-18"}),
    Q("range", observationIdsCount={"gte": 5}),
    Q("range", originator__count={"gte": 3}),
]))

count = og_search.count()
print(count)


# ogs = [og.meta.id for og in og_search.scan()]

# with open("eds_needs_pypedream_trip.csv", "w", newline="") as ogfile:
#     writer = csv.writer(ogfile)
#     for og in ogs:
#         writer.writerow([
#             og
#         ])

# counter = 0

ogs_and_rolls = []
for og in ogs:
    counter += 1
    print(counter)
    req_search = Search(
        using="staging", index="eds_image_request-*"
    ).filter(
        Q("term", obsGroupId=og)
    ).sort("-requestTs")[:1]
    try:
        ogs_and_rolls.append((og, req_search.execute().hits[0].rollupFile))
    except IndexError:
        print("wtf og", og)


# print(ogs_and_rolls[0])


with open("retry_file_reimage_true.csv", "w", newline="") as og_file:
    writer = csv.writer(og_file, delimiter=",")
    for og, roll in ogs_and_rolls:
        writer.writerow([
            roll.split("everest-prod-eds-rollup/")[1],
            og,
            "image"
        ])