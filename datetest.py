from aws_sso import boto3_client
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice

def time_windows(start_date, end_date, day_step=7):
    start_window = maya.when(start_date, timezone="UTC")
    print(start_window.day)
    end_window = start_window.add(days=day_step-1).snap("@d+23h+59m+59s")
    while end_window < maya.when(end_date):
        yield {
            "start": start_window.iso8601(),
            "end": end_window.iso8601()
        }
        start_window = start_window.add(days=day_step)
        end_window = end_window.add(days=day_step)
    yield {
        "start": start_window.iso8601(),
        "end": maya.when(end_date).snap("@d+23h+59m+59s").iso8601()
    }

for week in time_windows('2018-09-17', '2019-05-31'):
    pp('week')
    pp(week["start"])
    pp(week["end"])