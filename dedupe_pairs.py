import csv

with open('sender_date_pairs_missing_media.csv', newline='') as pair_file:
    pairs = set((row[0], row[1]) for row in csv.reader(pair_file, delimiter=','))

with open('sender_date_pairs_missing_media.csv', "w", newline='') as pair_file:
    pair_writer = csv.writer(pair_file, delimiter=',')
    for pair in pairs:
        pair_writer.writerow(list(pair))