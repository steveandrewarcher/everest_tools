from aws_sso import boto3_client
import json

kinesis_client = boto3_client('kinesis', region_name='us-east-2')
kinesis_client.describe_stream(StreamName="kin-ba-eds-obsgroup")


record = {
    "Data": json.dumps(
        {"This": "isn't", "a_real": "thing"}
    ),
    "PartitionKey": "191702805"
}

output = kinesis_client.put_records(
    StreamName="kin-ba-eds-obsgroup",
    Records=[record],
)

print(output)