from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime

QA = "readonly.elb-es-staging.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    qa={
        'hosts': [QA],
        'sniff_on_start': True
    }
)

srch = Search(
    using="qa", index="eds_orig_v2"
).source(["timebased.country"])

counter = 0
with open("originators.csv", "w", newline='') as csvfile:
    orig_writer = csv.writer(csvfile, delimiter=',')
    for orig in srch.scan():
        counter = counter + 1
        print(counter)
        orig_writer.writerow([
            orig.meta.id,
            orig.timebased[-1].country
        ])

