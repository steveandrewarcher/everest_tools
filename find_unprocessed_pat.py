from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from aws_sso import boto3_client


ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
)

srch = Search(
    using='analytics', index="eve"
).query(Q("bool", must=[
    Q("match", vendor="PAT"),
    ~Q("match", isUsable=False),
    ~Q("exists", field="scratch"),
    Q("match", og_o_join="og"),
    ~Q("match", channelType="PFB"),
])).source([])

print(srch.count())

with open("unprocessed_pat_ogs.csv", "w", newline='') as csvfile:
    og_writer = csv.writer(csvfile, delimiter=',')
    for og in srch.scan():
        og_writer.writerow([
            og.meta.id,
        ])
