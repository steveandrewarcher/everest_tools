from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime

PROCESS_START = datetime.datetime(2018,1,1)
ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    }
)

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

with open('originators.csv', newline='') as origs_file, open("eds_orig_mistakes.csv", "w", newline='') as mistakes:
    mistake_writer = csv.writer(mistakes, delimiter=',')
    orig_reader = csv.reader(origs_file, delimiter=',')
    origs_chunk = {}
    done = False
    counter = 0
    for row in orig_reader:
        origs_chunk[row[0]] = row[1]
        if len(origs_chunk) >= 10000:
            counter = counter + 1
            srch = Search(
                using='staging', index=f'eds_orig_v1'
            ).query(
                Q("ids", values=list(origs_chunk.keys()))
            ).source(["timebased.country","EDS"])
            print(f'searching chunk of {len(origs_chunk)} number {counter}')
            for prod_orig in srch.scan():
                fresh = True
                if "timebased" in prod_orig:
                    for event in prod_orig.EDS.demographics:
                        if prod_orig.meta.id == "EDS:1793079":
                            print('**************************************************')
                            print(datetime.datetime.strptime(event["end"],"%Y-%m-%dT%H:%M:%S"))
                            print(datetime.datetime.strptime(event["end"],"%Y-%m-%dT%H:%M:%S") < PROCESS_START)
                        if datetime.datetime.strptime(event["end"],"%Y-%m-%dT%H:%M:%S") < PROCESS_START:
                            fresh = False
                    if fresh:
                        if origs_chunk[prod_orig.meta.id] != prod_orig.timebased[-1].country:
                            mistake_writer.writerow([
                                prod_orig.meta.id,
                                origs_chunk[prod_orig.meta.id],
                                prod_orig.timebased[-1].country,
                            ])
            origs_chunk = {}
    # one last time
    srch = Search(
        using='staging', index=f'eds_orig_v1'
    ).query(
        Q("ids", values=list(origs_chunk.keys()))
    ).source(["timebased.country","EDS"])
    print(f'last searcgh, searching chunk of {len(origs_chunk)}')
    for prod_orig in srch.scan():
        if "timebased" in prod_orig:
            for event in prod_orig.EDS.demographics:
                if datetime.datetime.strptime(event["end"],"%Y-%m-%dT%H:%M:%S") < PROCESS_START:
                    continue
            if origs_chunk[prod_orig.meta.id] != prod_orig.timebased[-1].country:
                mistake_writer.writerow([
                    prod_orig.meta.id,
                    origs_chunk[prod_orig.meta.id],
                    prod_orig.timebased[-1].country,
                ])