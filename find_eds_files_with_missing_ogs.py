import csv
from collections import defaultdict

obs_domains_map = defaultdict(lambda: [])
with open('obs_and_domains.csv') as missing_ogs_file:
    missing_og_reader = csv.reader(missing_ogs_file, delimiter=',')
    for row in missing_og_reader:
        obs_domains_map[row[1]].append(row[2])

obs_domains_set = set(obs_domains_map.keys())

og_domain_set = set()
with open('ogs_and_domains.csv') as missing_ogs_file:
    missing_og_reader = csv.reader(missing_ogs_file, delimiter=',')
    for row in missing_og_reader:
        og_domain_set.add(row[1])

missing_domain_set = obs_domains_set - og_domain_set

print(len(missing_domain_set))
print(list(missing_domain_set)[:5])

file_set = set()
for domain in missing_domain_set:
    for file_name in obs_domains_map[domain]:
        file_set.add(file_name)

print(len(file_set))

with open("eds_files_to_reprocess_missing_domains.csv", "w", newline='') as csvfile:
    file_writer = csv.writer(csvfile, delimiter=',')
    for file_name in file_set:
        file_writer.writerow([
            f's3://everest-s3-shared-ue2-eds-incoming/{file_name}'
        ])