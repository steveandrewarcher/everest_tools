from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

analytics_og_search = Search(
    using='analytics', index="eve-201911"
).filter(Q("bool", must=[
    #Q("range", startDate={"gte": "2018-01-01", "lte": "2019-12-17T23:59:59"}),
    Q("term", og_o_join="og"),
    Q("term", vendor="EDS"),
    #~Q("term", channelType="PFB"),
    Q("exists", field="isPublishedOps")
]))

print(analytics_og_search.count())


staging_og_search = Search(
    using='staging', index="eds_og-201911"
).filter(Q("bool", must=[
    #Q("range", endDate={"gte": "2018-01-01", "lte": "2019-12-17T23:59:59"}),
    Q("exists", field="isPublishedOps")
])).source(["email", "sent"])

print(staging_og_search.count())

a_ogs = set(og.meta.id for og in analytics_og_search.scan())
print("a done")
s_ogs = {og.meta.id: (og.email, og.sent) for og in staging_og_search.scan()}
print("s done")

missing_ogs = set(s_ogs.keys()) - a_ogs
missing_pairs = set([s_ogs[og] for og in missing_ogs])

with open("eds_og_PAIRS_in_staging_but_not_analytics_201911_try_again.csv", "w", newline="") as wthfile:
    wthwriter = csv.writer(wthfile, delimiter=',')
    for pair in missing_pairs:
        wthwriter.writerow(list(pair))