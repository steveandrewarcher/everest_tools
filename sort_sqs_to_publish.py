from aws_sso import boto3_client, boto3_resource
import json
import csv
import time
from collections import defaultdict
import pdb
# queue_name = 'publish-expert-input-queue.fifo'
# sqs = boto3_resource('sqs')
# queue = sqs.get_queue_by_name(QueueName=queue_name)
# delete_count = 0

with open('publish_messages_to_requeue.csv', newline='') as message_file:
    message_reader = csv.reader(message_file, delimiter=",")
    ogs_to_reprocess = set()
    for message in message_reader:
        body = json.loads(message[1])
        og_id = body["payload"]["og_id"]
        ogs_to_reprocess.add(og_id)

with open("ogs_from_sqs_to_reprocess.csv", "w", newline='') as dupe_file:
    dupe_writer = csv.writer(dupe_file, delimiter=",")
    for og in ogs_to_reprocess:
        dupe_writer.writerow([og])


print("done")

