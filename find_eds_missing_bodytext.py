import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

search = Search(
    using="staging", index="eds_og"
).filter(Q("bool", must=[
    ~Q("exists", field="media.fullText-eds-body-text"),
    ~Q("exists", field="media.edsImage"),
    ~Q("exists", field="media.eds-image"),
    Q("match", isUsable=True),
    Q("exists", field="media.image.url"),
])).source(["email", "sent"])

print(search.count())

with open("eds_ogs_missing_bodytest.csv", "w", newline='') as missingfile:
    missingwriter = csv.writer(missingfile, delimiter=',')
    pairs = set((og.email, og.sent) for og in search.scan())

    for pair in pairs:
        missingwriter.writerow(list(pair))
