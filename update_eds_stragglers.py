from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

with open("eds_og_ids_without_recordType.csv", newline='') as stragglers_file:
    straggler_reader = csv.reader(stragglers_file, delimiter=',')
    og_map = {row[2]: (row[0], row[1]) for row in straggler_reader}


counter = 0
pairs = set()
for chunk in grouper(og_map, 1000):
    counter += 1
    print(f"chunk {counter}")
    og_search = Search(
        using='analytics', index="eve"
    ).filter(Q("ids", values=list(chunk))).source([])

    for og in og_search.scan():
        pairs.add(og_map[og.meta.id])
        
with open("found_in_analytics.csv", "w", newline='') as found_file:
    found_writer = csv.writer(found_file, delimiter=',')
    for pair in pairs:      
        found_writer.writerow([pair[0], pair[1]])



