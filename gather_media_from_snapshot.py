import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"
QA_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    },
    qa_analytics={
        'hosts': [QA_ANALYTICS],
        'sniff_on_start': True
    }
)


with open("all_eds_ogs_in_staging_missing_media.csv", newline='') as nomedia_file1:
    ogs_with_no_media = [list(row) for row in csv.reader(nomedia_file1, delimiter=",")]
print(len(ogs_with_no_media))
media_map = defaultdict(dict)
counter = 0
for chunk in grouper(ogs_with_no_media, 500):
    counter += 500
    print(counter)
    pair_map = {thing[0]: [thing[1], thing[2]] for thing in chunk}
    snapshot_search = Search(
        using="qa_analytics", index="restoreeve-*"
    ).filter(Q('bool', must=[
        Q("ids", values=list(pair_map.keys())),
        Q("exists", field="media")
    ])).source(["media"])
    for og in snapshot_search.scan():
        try:
            key = tuple([og.meta.id, pair_map[og.meta.id][0], pair_map[og.meta.id][1]])
            media_map[key] = og.media.to_dict()
        except:
            print(key)
            print(og.media.to_dict())

with open("media_from_snapshot_for_all_eds_ogs_in_staging_missing_media.csv", "w", newline='') as media_file:
    media_writer = csv.writer(media_file, delimiter=',')
    for key, media in media_map.items():
        media_writer.writerow([
            key[0],
            key[1],
            key[2],
            json.dumps(media)
        ])
