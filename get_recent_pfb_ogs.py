from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict

ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

with open("try_again.csv", newline='') as ogs_file:
    og_reader = csv.reader(ogs_file, delimiter=',')
    og_ids = [og[0] for og in og_reader]
print(len(og_ids))

counter = 0
with open("more_actual_bad_pfb_ogs.csv", "w", newline='') as bad_file:
    with open("try_again_AGAIN.csv", "w+", newline='') as try_file:
        try_writer = csv.writer(try_file, delimiter=',')

        bad_writer = csv.writer(bad_file, delimiter=',')
        for chunk in grouper(og_ids, 1000):
            counter +=1
            print(f'chunk {counter}')
            og_search = Search(
                using='analytics', index="eve"
            ).filter(
                Q("bool", must=[
                    Q("ids", values=list(chunk)),
                    Q("exists", field="media"),
                    ~Q("exists", field="isPublishedOps"),
                    Q("match", isUsable=True)
                ])
            ).source([])
            print(og_search.count())
            try:
                for og in og_search.scan():
                    bad_writer.writerow([og.meta.id])
            except:
                print("oops")
                for og in chunk:
                    try_writer.writerow([og])

