import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

with open('eds_in_staging_but_not_analytics.txt', newline='') as load_file:
    og_ids = [row[0] for row in csv.reader(load_file)]

pairs = []
for chunk in grouper(og_ids, 500):
    search = Search(
        using='staging', index='eds_og'
    ).filter(Q('ids', values=list(chunk))).source(["email", "sent"])
    pairs.extend([(og.email, og.sent) for og in search.scan()])

pairs_set = set(pairs)

with open('eds_pairs_in_staging_but_not_analytics.csv', "w", newline='') as pair_file:
    pair_writer = csv.writer(pair_file, delimiter=',')
    for pair in pairs_set:
        pair_writer.writerow(list(pair))