from aws_sso import boto3_client, boto3_resource
import json
from datetime import datetime, timedelta
import csv


s3 = boto3_resource("s3")
s3_bucket = s3.Bucket("customer-mintel")

for bucket in s3.buckets.all():
  print(bucket.name)