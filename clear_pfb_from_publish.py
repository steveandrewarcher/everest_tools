from aws_sso import boto3_client, boto3_resource
import json
import csv

def should_requeue(body):
    payload = body["payload"]
    return (
        payload['og_id'][:7] != "PAT:PFB"
    )

queue_name = 'publish-expert-input-queue.fifo'
sqs = boto3_resource('sqs')
queue = sqs.get_queue_by_name(QueueName=queue_name)
delete_count = 0
print("hi")
while True:
    messages_to_delete = []
    messages_to_requeue = []
    for message in queue.receive_messages(
        MaxNumberOfMessages=10,
        VisibilityTimeout=300,
        AttributeNames=["MessageGroupId"]
    ):
        if message.attributes["MessageGroupId"] == '0':
            body = json.loads(message.body)
            messages_to_delete.append({
                'Id': message.message_id,
                'ReceiptHandle': message.receipt_handle
            })
            if should_requeue(body):
                messages_to_requeue.append({
                    'Id': message.message_id,
                    'MessageBody': message.body,
                    'MessageGroupId': body["payload"]["og_id"]
                })
    if messages_to_delete:
        delete_count += len(messages_to_delete)
        delete_response = queue.delete_messages(
            Entries=messages_to_delete
        )
        print(delete_count)
    else:
        print("found nothing")
    if messages_to_requeue:
        requeue_response = queue.send_messages(
            Entries=messages_to_requeue
        )
print("done")

