from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


def grouper(iterable, n):
    """
    Groups an iterable into chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, n))
        if not chunk:
            return
        yield chunk

ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

# obs_search = Search(
#         using='staging', index="eds_o-201910"
#     ).query(
#         "wildcard",
#         channelBatchId="*XXX.tsv",
#     ).query(Q("bool", must=[
#         Q("range", ingestionTs={"gte": "2019-11-13T00:00:00", "lte": "2019-11-13T23:23:59"}),
#         Q("range", obsDate={"gte": "2019-10-24T00:00:00", "lte": "2019-10-26T23:23:59"})
#     ])).source(['obsGroupId'])

# print(obs_search.count())


og_search = Search(
    using='staging', index="eds_og-201910"
).query(Q("bool", must=[
    Q("range", ingestionTs={"gte": "2019-11-13T00:00:00", "lte": "2019-11-13T23:23:59"}),
    Q("range", endDate=({"gte": "2019-10-24T00:00:00", "lte": "2019-10-26T23:23:59"}))
])).source(["observationIds"])

print(og_search.count())

og_and_obs_map = {}
counter = 0
for og in og_search.scan():
    counter += 1
    print(f"scan ogs number {counter}")
    og_and_obs_map[og.observationIds[0]] = og.meta.id
print(list(og_and_obs_map.values())[0])

bad_ogs = set()
counter = 0
for obs in og_and_obs_map.keys():
    counter += 1
    print(f"do obs search number {counter}")
    obs_search = Search(
        using='staging', index="eds_o-201910"
    ).query(
        Q("ids", values=[obs])
    )
    if obs_search.count() == 0:
        bad_ogs.add(og_and_obs_map[obs])

print(len(bad_ogs))
print(list(bad_ogs)[0])

count = 0
analytics_count = 0
for chunk in grouper(list(bad_ogs), 500):
    print(f"doing chunk {count}")
    og_search = Search(
        using='staging', index="eds_og-201910"
    ).query(
        "ids", values=list(chunk)
    )

    analytics_og_search = Search(
        using='analytics', index="eve-201910"
    ).query(
        "ids", values=list(chunk)
    )

    count += og_search.count()
    analytics_count += analytics_og_search.count()
    og_search.delete()
    analytics_og_search.delete()

print(count)
print(analytics_count)
