import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

QA_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    },
    qa_analytics={
        'hosts': [QA_ANALYTICS],
        'sniff_on_start': True
    }
)

search = Search(
    using="analytics", index="eve"
).filter(Q("bool", must=[
    Q("range", endDate={"gte":"2018-01-01"}),
    Q("term", og_o_join="og"),
    Q("term", vendor="PAT"),
    ~Q("exists", field="scratch"),
    ~Q("term", channelType="PFB")
]))

print(search.count())

with open("pat_missing_media.csv", "w", newline='') as missfile:
    misswriter = csv.writer(missfile, delimiter=',')
    for og in search.scan():
        misswriter.writerow([og.meta.id])