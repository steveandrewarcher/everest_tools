import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

search = Search(
    using="staging", index="eds_og"
).filter(Q("bool", must=[
    Q("range", endDate={"gte": "2018-01-01"}),
    ~Q("exists", field="scratch"),
    ~Q("match", isUsable=False),
    Q("range", observationIdsCount={"gt": 4}),
    Q("range", originator__count={"gt": 2}),
    Q("exists", field="media.image.url"),
    Q("match", recordType="og")
])).source(["email", "sent"])

print(search.count())

with open("eds_ogs_unprocessed_pairs.csv", "w", newline='') as pairsfile:
    with open("eds_ogs_unprocessed.csv", "w", newline='') as missingfile:
        pairswriter = csv.writer(pairsfile, delimiter=',')
        missingwriter = csv.writer(missingfile, delimiter=',')
        ogs = set()
        pairs = set()
        for og in search.scan():
            pairs.add((og.email,og.sent))
            missingwriter.writerow([og.meta.id])

        for pair in pairs:
            pairswriter.writerow(list(pair))
