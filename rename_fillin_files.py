from aws_sso import boto3_client, boto3_resource
import json
from datetime import datetime, timedelta
import csv

# s3_client = boto3_client('s3', region_name='us-east-2')
# s3 = boto3_resource("s3")
# s3_bucket = s3.Bucket("everest-s3-shared-ue2-eds-incoming")


# buckets = s3.buckets.all()
# for b in buckets:
#     print(b)

with open("better_eds_fillin_files.csv", "w", newline='') as new_csvfile:
    with open("eds_fillin_files.csv", newline='') as csvfile:
        filereader = csv.reader(csvfile, delimiter=',')
        filewriter = csv.writer(new_csvfile, delimiter=",")
        for old_path in filereader:
            old_key = old_path[0][40:]
            print(old_key)
            parts = old_key.split("/")
            new_date = parts[2]
            old_filename = parts[3]

            new_filename = old_filename[:7] + new_date + old_filename[15:]

            print(new_filename)

            parts[3] = new_filename

            new_key = "/".join(parts)
            print(new_key)
            new_path = old_path[0][:40] + new_key
            print(new_path)
            filewriter.writerow([new_path])
            #s3.Object("everest-s3-shared-ue2-eds-incoming", new_key).copy_from(CopySource = {'Bucket': "everest-s3-shared-ue2-eds-incoming", 'Key': old_key})
            # s3_client.copy_object(
            #     CopySource={'Bucket': "everest-s3-shared-ue2-eds-incoming", 'Key': old_key},
            #     Bucket="everest-s3-shared-ue2-eds-incoming",
            #     Key=new_key,
            # )


        # for obj in s3_bucket.objects.filter(
        #     Prefix=f"v4.2/fix_20191024/email/20180101/emails-20191024230221-XXXX.tsv"
        # ):
        #     print(obj.key)

