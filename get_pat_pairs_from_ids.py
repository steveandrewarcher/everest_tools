from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

with open('stuff_missing_from_staging.csv', newline='') as ogfile:
    og_reader = csv.reader(ogfile, delimiter=',')
    og_ids = set(og for og in og_reader)

og_search = Search(using='analytics', index='eve').filter(
    Q('ids', values=list(og_ids))
).source(["PAT.creativeId", "PAT.device"])

with open("pairs_missing_from_staging.csv", "w", newline='') as pairfile:
    pair_writer = csv.writer(pairfile, delimiter=',')

    for og in og_search.scan():
        pair_writer([og.PAT["creativeId"], og.PAT["device"]])