RATE_FIELDS = [
    "rate_delete",
    "rate_delete___aol",
    "rate_delete___google",
    "rate_delete___hotmail",
    "rate_delete___yahoo",
    "rate_delete___other",
    "rate_delete___ca",
    "rate_delete___ca_aol",
    "rate_delete___ca_google",
    "rate_delete___ca_hotmail",
    "rate_delete___ca_yahoo",
    "rate_delete___ca_other",
    "rate_delete___us",
    "rate_delete___us_aol",
    "rate_delete___us_google",
    "rate_delete___us_hotmail",
    "rate_delete___us_yahoo",
    "rate_delete___us_other",
    "rate_delete___xx",
    "rate_delete___xx_aol",
    "rate_delete___xx_google",
    "rate_delete___xx_hotmail",
    "rate_delete___xx_yahoo",
    "rate_delete___xx_other",
    "rate_delete___zz",
    "rate_delete___zz_aol",
    "rate_delete___zz_google",
    "rate_delete___zz_hotmail",
    "rate_delete___zz_yahoo",
    "rate_delete___zz_other",
    "rate_delete_without_open",
    "rate_delete_without_open___aol",
    "rate_delete_without_open___google",
    "rate_delete_without_open___hotmail",
    "rate_delete_without_open___yahoo",
    "rate_delete_without_open___other",
    "rate_delete_without_open___ca",
    "rate_delete_without_open___ca_aol",
    "rate_delete_without_open___ca_google",
    "rate_delete_without_open___ca_hotmail",
    "rate_delete_without_open___ca_yahoo",
    "rate_delete_without_open___ca_other",
    "rate_delete_without_open___us",
    "rate_delete_without_open___us_aol",
    "rate_delete_without_open___us_google",
    "rate_delete_without_open___us_hotmail",
    "rate_delete_without_open___us_yahoo",
    "rate_delete_without_open___us_other",
    "rate_delete_without_open___xx",
    "rate_delete_without_open___xx_aol",
    "rate_delete_without_open___xx_google",
    "rate_delete_without_open___xx_hotmail",
    "rate_delete_without_open___xx_yahoo",
    "rate_delete_without_open___xx_other",
    "rate_delete_without_open___zz",
    "rate_delete_without_open___zz_aol",
    "rate_delete_without_open___zz_google",
    "rate_delete_without_open___zz_hotmail",
    "rate_delete_without_open___zz_yahoo",
    "rate_delete_without_open___zz_other",
    "rate_inbox",
    "rate_inbox___aol",
    "rate_inbox___google",
    "rate_inbox___hotmail",
    "rate_inbox___yahoo",
    "rate_inbox___other",
    "rate_inbox___ca",
    "rate_inbox___ca_aol",
    "rate_inbox___ca_google",
    "rate_inbox___ca_hotmail",
    "rate_inbox___ca_yahoo",
    "rate_inbox___ca_other",
    "rate_inbox___us",
    "rate_inbox___us_aol",
    "rate_inbox___us_google",
    "rate_inbox___us_hotmail",
    "rate_inbox___us_yahoo",
    "rate_inbox___us_other",
    "rate_inbox___xx",
    "rate_inbox___xx_aol",
    "rate_inbox___xx_google",
    "rate_inbox___xx_hotmail",
    "rate_inbox___xx_yahoo",
    "rate_inbox___xx_other",
    "rate_inbox___zz",
    "rate_inbox___zz_aol",
    "rate_inbox___zz_google",
    "rate_inbox___zz_hotmail",
    "rate_inbox___zz_yahoo",
    "rate_inbox___zz_other",
    "rate_open",
    "rate_open___aol",
    "rate_open___google",
    "rate_open___hotmail",
    "rate_open___yahoo",
    "rate_open___other",
    "rate_open___ca",
    "rate_open___ca_aol",
    "rate_open___ca_google",
    "rate_open___ca_hotmail",
    "rate_open___ca_yahoo",
    "rate_open___ca_other",
    "rate_open___us",
    "rate_open___us_aol",
    "rate_open___us_google",
    "rate_open___us_hotmail",
    "rate_open___us_yahoo",
    "rate_open___us_other",
    "rate_open___xx",
    "rate_open___xx_aol",
    "rate_open___xx_google",
    "rate_open___xx_hotmail",
    "rate_open___xx_yahoo",
    "rate_open___xx_other",
    "rate_open___zz",
    "rate_open___zz_aol",
    "rate_open___zz_google",
    "rate_open___zz_hotmail",
    "rate_open___zz_yahoo",
    "rate_open___zz_other",
    "rate_spam",
    "rate_spam___aol",
    "rate_spam___google",
    "rate_spam___hotmail",
    "rate_spam___yahoo",
    "rate_spam___other",
    "rate_spam___ca",
    "rate_spam___ca_aol",
    "rate_spam___ca_google",
    "rate_spam___ca_hotmail",
    "rate_spam___ca_yahoo",
    "rate_spam___ca_other",
    "rate_spam___us",
    "rate_spam___us_aol",
    "rate_spam___us_google",
    "rate_spam___us_hotmail",
    "rate_spam___us_yahoo",
    "rate_spam___us_other",
    "rate_spam___xx",
    "rate_spam___xx_aol",
    "rate_spam___xx_google",
    "rate_spam___xx_hotmail",
    "rate_spam___xx_yahoo",
    "rate_spam___xx_other",
    "rate_spam___zz",
    "rate_spam___zz_aol",
    "rate_spam___zz_google",
    "rate_spam___zz_hotmail",
    "rate_spam___zz_yahoo",
    "rate_spam___zz_other",
    "rate_ca_aol",
    "rate_ca_google",
    "rate_ca_hotmail",
    "rate_ca_yahoo",
    "rate_ca_other",
    "rate_us_aol",
    "rate_us_google",
    "rate_us_hotmail",
    "rate_us_yahoo",
    "rate_us_other",
    "rate_xx_aol",
    "rate_xx_google",
    "rate_xx_hotmail",
    "rate_xx_yahoo",
    "rate_xx_other",
    "rate_zz_aol",
    "rate_zz_google",
    "rate_zz_hotmail",
    "rate_zz_yahoo",
    "rate_zz_other",
]