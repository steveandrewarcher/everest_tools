from aws_sso import boto3_client, boto3_resource
from datetime import datetime
import pytz
from more_itertools import chunked

cdt=pytz.UTC

s3 = boto3_client('s3', region_name='us-east-1')
end = cdt.localize(datetime(2020, 6, 25))
start_after = None

ks = {
    "Bucket": 'ym-mintel-data',
    "Prefix": "restatement"
}

response = s3.list_objects_v2(**ks)

keys_to_copy = [obj['Key'] for obj in response['Contents']]

print(len(keys_to_copy))
print(keys_to_copy[:3])

counter = 0
for key in keys_to_copy:
    counter += 1
    print(counter)
    s3.copy(
        {
            'Bucket': 'ym-mintel-data',
            'Key': key
        },
        "everest-s3-shared-ue2-pat-incoming",
        key
    )