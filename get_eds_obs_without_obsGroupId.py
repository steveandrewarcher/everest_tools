from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

obs_search = Search(
        using='staging', index="eds_o"
    ).query(Q("bool", must=[
        Q("range", obsDate={"gte": "2018-01-01T00:00:00", "lte": "2019-12-10T23:23:59"}),
        ~Q("exists", field='obsGroupId'),
        Q("match", isUsable=True)
    ])).source(['sent', "email"])

print(obs_search.count())

pairs = set()
counter = 0
for obs in obs_search.scan():
    counter += 1
    if counter % 1000 == 0:
        print(counter)
    pairs.add((obs.email, obs.sent))

print(len(pairs))

with open("eds_obs_without_obsGroupId.csv", "w", newline='') as csvfile:
    obs_writer = csv.writer(csvfile, delimiter=',')
    for pair in pairs:
        obs_writer.writerow([
            pair[0],
            pair[1]
        ])

