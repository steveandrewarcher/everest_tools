from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
import sys


csv.field_size_limit(sys.maxsize)


ES_STAGING = "elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)


analytics_operations = []
staging_operations = []
with open("media_from_snapshot_for_dupes_in_staging.csv", newline='') as mediafile:
    media_reader = csv.reader(mediafile, delimiter=',')
    for row in media_reader:
        pair = json.loads(row[0])[1]
        routing = f"{pair[0]}:{pair[1]}"
        index_suffix = pair[1][:6]

        media = json.loads(row[1])
        eds_image = media.pop("eds-image", None)
        if "edsImage" not in media and eds_image:
            media["edsImage"] = {
                "imageId": eds_image.get("image_id"),
                "emailIds": eds_image.get("email_ids"),
                "fromAddress": eds_image.get("from_address"),
                "url": eds_image.get("url")
            }

        staging_operations.append({
            '_op_type': 'update',
            '_index': f"eds_og-{index_suffix}",
            '_type': 'doc',
            '_id': json.loads(row[0])[0],
            '_routing': routing,
            'doc': {
                "media": media
            }
        })
        analytics_operations.append({
            '_op_type': 'update',
            '_index': f"eve-{index_suffix}",
            '_type': 'doc',
            '_id': json.loads(row[0])[0],
            '_routing': routing,
            'doc': {
                "media": media
            }
        })

print(len(analytics_operations))
print(analytics_operations[0])

analytics_result = bulk(connections.get_connection('analytics'), analytics_operations, raise_on_error=False)
print(analytics_result[0])
print(len(analytics_result[1]))
with open("cant_restore_media_not_in_analytics.csv", "a+", newline='') as missingfile:
    missing_writer = csv.writer(missingfile, delimiter=',')
    for thing in analytics_result[1]:
        if thing["update"]["status"] == 404 and thing["update"]["error"]["type"] == "document_missing_exception":
            missing_writer.writerow([thing["update"]["_id"]])
        else:
            print(thing)

staging_result = bulk(connections.get_connection('staging'), staging_operations, raise_on_error=False)
print(staging_result[0])
print(len(staging_result[1]))
with open("cant_restore_media_not_in_staging.csv", "a+", newline='') as missingfile:
    missing_writer = csv.writer(missingfile, delimiter=',')
    for thing in staging_result[1]:
        if thing["update"]["status"] == 404 and thing["update"]["error"]["type"] == "document_missing_exception":
            missing_writer.writerow([thing["update"]["_id"]])
        else:
            print(thing)

