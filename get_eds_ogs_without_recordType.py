from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    }
)

og_search = Search(
    using='staging', index="eds_og"
).filter(
    Q("bool", must=[
        ~Q("exists", field="recordType"),
        Q("match", isUsable=True)
    ])
).source(["email", "sent"]).params(request_timeout=600)

og_count = og_search.count()
print(og_count)

pairs = set()
counter = 0
ids = set()
for og in og_search.scan():
    counter += 1
    if counter % 1000 == 0:
        print(f"{counter} out of {og_count}")
    #pairs.add((og.email, og.sent))
    ids.add((og.email, og.sent, og.meta.id))

with open("eds_og_ids_without_recordType.csv", "w", newline='') as idfile:
    id_writer = csv.writer(idfile, delimiter=',')
    for id in ids:
        id_writer.writerow([
            thing for thing in id
        ])

# with open("eds_ogs_without_recordType.csv", "w", newline='') as csvfile:
#     og_writer = csv.writer(csvfile, delimiter=',')
#     for pair in pairs:
#         og_writer.writerow([
#             pair[0],
#             pair[1]
#         ])

