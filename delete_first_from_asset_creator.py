from aws_sso import boto3_client, boto3_resource
import json
import csv
import time

queue_name = 'asset-creator-expert-input-queue.fifo'
sqs = boto3_resource('sqs')
queue = sqs.get_queue_by_name(QueueName=queue_name)
delete_count = 0

with open('message_from_asset_creator.csv', "a+", newline='') as message_file:
    message_writer = csv.writer(message_file, delimiter=",")
    while int(queue.attributes['ApproximateNumberOfMessages']) > 0:
        messages_to_delete = []
        for message in queue.receive_messages(
            MaxNumberOfMessages=1,
        ):
            message_writer.writerow([message.message_id, message.body])
            messages_to_delete.append({
                'Id': message.message_id,
                'ReceiptHandle': message.receipt_handle
            })

        if messages_to_delete:
            delete_count += len(messages_to_delete)
            delete_response = queue.delete_messages(
                Entries=messages_to_delete
            )
            print(delete_count)
            break
        else:
            print("found nothing")
        queue.reload()
print("done")

