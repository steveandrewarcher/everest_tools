from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from dateutil.parser import parse
from collections import defaultdict
from utils import grouper

ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)

with open("stuck_pat_ogs.csv", "w", newline="") as so_naughty:
    so_naughty_writer = csv.writer(so_naughty)
    search = Search(
        using="staging", index='pat_og'
    ).filter(Q("bool", must=[
        Q("bool", must=[
            Q("range", endDate={"gte": "2018-01-01"}),
            ~Q("exists", field="isPublishedOps"),
            Q("match", isUsable=True)
        ]),
    ])).source([])
    print(search.count())
    counter = 0
    for og in search.scan():
        counter =+ 1
        if counter % 1000 == 0:
            print(counter)
        so_naughty_writer.writerow([og.meta.id])