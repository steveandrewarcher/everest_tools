from aws_sso import boto3_client, boto3_resource
import json
from datetime import datetime, timedelta
import csv

s3_client = boto3_client('s3', region_name='us-east-2')
s3 = boto3_resource("s3")
s3_bucket = s3.Bucket("everest-s3-shared-ue2-eds-incoming")


# buckets = s3.buckets.all()
# for b in buckets:
#     print(b)

with open("eds_fillin_files.csv", newline='') as csvfile:
    filereader = csv.reader(csvfile, delimiter=',')

    for old_path in filereader:
        old_key = old_path[0][40:]
        s3_client.delete_object(
            Bucket="everest-s3-shared-ue2-eds-incoming",
            Key=old_key
        )