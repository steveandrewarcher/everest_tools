import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
QA_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-qa.aws.mintel.com:9200"
PROD_ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"
ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    qa_analytics={
        'hosts': [QA_ES_ANALYTICS],
        'sniff_on_start': True
    },
    prod_analytics={
        'hosts': [PROD_ES_ANALYTICS],
        'sniff_on_start': True
    },
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
)


with open("still_screwy_eds_201809.csv", newline='') as screwyfile:
    screwy = [row[0] for row in csv.reader(screwyfile, delimiter=',')]

search = Search(
    using="qa_analytics", index='restoreeve-*'
).filter(Q("bool", must=[
    Q("ids", values=screwy),
    Q("range", observationIdsCount={"gte": 5})
]))

print(search.count())

with open("still_screwy_eds_201809_greather.csv", "w", newline='') as screwyfile:
    screwy_writer = csv.writer(screwyfile, delimiter=',')
    counter = 0
    for og in search.scan():
        counter += 1
        if counter % 100 == 0:
            print(counter)
        screwy_writer.writerow([og.meta.id])