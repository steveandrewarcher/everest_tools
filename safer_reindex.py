from elasticsearch import Elasticsearch
from elasticsearch_dsl import connections
from elasticsearch.client.ingest import IngestClient
from rate_fields import RATE_FIELDS
import time

connections.configure(
    staging={
        "hosts": ["http://elb-es-staging.int.everest-prod.aws.mintel.com:9200"],
        "timeout": 60,
    },
    analytics={
        "hosts": ["http://elb-es-analytics.int.everest-prod.aws.mintel.com:9200"],
        "timeout": 60,
    },
)


staging = connections.get_connection("staging")

orig_index = "eds_og-201911"
new_index = f"{orig_index}-temp"

p = IngestClient(staging)

processors = [
    {
        "script": {
            "lang": "painless",
            "id": f"edsratescript_params",
            "params": {
                "field_name": field
            }
        }
    } for field in RATE_FIELDS
]

processors.append({
    "script": {
        "lang": "painless",
        "source": "ctx._routing = ctx.email + ':' + ctx.sent"
    }
})
p.put_pipeline(id='convertEDSmetricsfields', body={
    "description": "converts rate fields",
    "processors": processors
})
# print("did it?")
# print(f"reindexing {orig_index} to {new_index}")
# staging.reindex(
#     body={"source": {"index": orig_index}, "dest": {"index": new_index, "pipeline": "convertEDSmetricsfields"}},
#     wait_for_completion=False,
#     refresh=True,
# )

# time.sleep(30)
# complete = False
# while not complete:
#     print("check if complete")
#     orig_r = staging.search(index=orig_index, size=0)
#     new_r = staging.search(index=new_index, size=0)
#     if orig_r["hits"]["total"] <= new_r["hits"]["total"]:
#         complete = True
#     print(new_r["hits"]["total"], orig_r["hits"]["total"])
#     time.sleep(60)


# print("done!")
# print("check again for extra safety")
# orig_r = staging.search(index=orig_index, size=0)
# new_r = staging.search(index=new_index, size=0)
# assert (
#     orig_r["hits"]["total"] <= new_r["hits"]["total"]
# ), "Totals between indices don't match"
# total = orig_r["hits"]["total"]
# print(f"{total}")


# staging.indices.delete(index=orig_index)


# print(f"reindexing {new_index} to {orig_index}")
# staging.reindex(
#     body={"source": {"index": new_index}, "dest": {"index": orig_index}},
#     wait_for_completion=False,
#     refresh=True,
# )

# time.sleep(30)
# complete = False
# while not complete:
#     print("check if complete")
#     orig_r = staging.search(index=orig_index, size=0)
#     new_r = staging.search(index=new_index, size=0)
#     if orig_r["hits"]["total"] == new_r["hits"]["total"]:
#         complete = True
#     print(orig_r["hits"]["total"], new_r["hits"]["total"])
#     time.sleep(60)

# print("done!")
# print("check again for extra safety")
# orig_r = staging.search(index=orig_index, size=0)
# new_r = staging.search(index=new_index, size=0)
# assert (
#     orig_r["hits"]["total"] == new_r["hits"]["total"]
# ), "Totals between indices don't match"
# total = orig_r["hits"]["total"]
# print(f"{total}")
# staging.indices.delete(index=new_index)
# print("cleaned up")
