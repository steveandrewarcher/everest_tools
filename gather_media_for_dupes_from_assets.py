import csv
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Q, A
from pprint import pprint as pp
import json
import maya
from itertools import islice
import csv
import datetime
from collections import defaultdict
from utils import grouper
import sys

csv.field_size_limit(sys.maxsize)


ES_STAGING = "readonly.elb-es-staging.int.everest-prod.aws.mintel.com:9200"
ES_ANALYTICS = "readonly.elb-es-analytics.int.everest-prod.aws.mintel.com:9200"

ES_ASSETS = "elb-es-static-assets.int.everest-shared.aws.mintel.com:9200"

connections.configure(
    staging={
        'hosts': [ES_STAGING],
        'sniff_on_start': True
    },
    analytics={
        'hosts': [ES_ANALYTICS],
        'sniff_on_start': True
    },
    assets={
        'hosts': [ES_ASSETS],
        'sniff_on_start': True
    }
)

def build_media(asset):
    if asset.mediaType == "image":
        if "url" in asset:
            return {
                asset.mediaType: {
                    "vendorUrl": asset.vendorUrl,
                    "fileExtensionMismatch": asset.fileExtensionMismatch,
                    "fileSize": asset.fileSize,
                    "width": asset.width,
                    "mimetype": asset.mimetype,
                    "createdInEnv": asset.createdInEnv,
                    "resolution": asset.resolution,
                    "url": asset.url,
                    "height": asset.height
                }
            }
    if asset.mediaType == "edsImage":
        if "emailIds" in asset:
            return {
                asset.mediaType: {
                    "emailIds": list(asset.emailIds),
                    "imageId": asset.imageId,
                    "url": asset.url
                }
            }
        else:
            return {
                asset.mediaType: {
                    "emailIds": list(asset.email_ids),
                    "imageId": asset.image_id,
                    "url": asset.url
                }
            }
    if asset.mediaType == "fullText-eds-body-text":
        return {
            asset.mediaType: {
                "mimetype": asset.mimetype,
                "encoding": asset.encoding,
                "createdInEnv": asset.createdInEnv,
                "content": asset.content,
                "url": asset.url
            }
        }
    return {}

with open("both_dupes_have_media.csv", "a+", newline='') as bothfile:
    both_writer = csv.writer(bothfile, delimiter=',')
    with open("dem_dupe_groups_in_staging.csv", newline='') as dupefile:
        dupe_reader = csv.reader(dupefile, delimiter=",")
        media_map = defaultdict(dict)
        counter = 0
        for row in dupe_reader:
            counter += 1
            if counter % 10 == 0:
                print(counter)
            ogs_plus_pairs = [(json.loads(og_and_pair)[0], tuple(json.loads(og_and_pair)[1])) for og_and_pair in row]

            ogs = [og_plus_pair[0] for og_plus_pair in ogs_plus_pairs]
            assets_search = Search(
                using="assets", index="assets-*"
            ).filter(Q('bool', must=[
                Q("terms", parentId=ogs),
                Q("terms", mediaType=["edsImage", "fullText-eds-body-text", "image"])
            ]))
            found_assets = [build_media(asset) for asset in assets_search.scan()]
            if len([asset for asset in found_assets if "image" in asset.keys()]) > 1:
                both_writer.writerow(ogs_plus_pairs)
            for og_plus_pair in ogs_plus_pairs:
                for asset in found_assets:
                    media_map[og_plus_pair].update(asset)

with open("media_from_assets_for_dupes_in_staging", "w", newline='') as media_file:
    media_writer = csv.writer(media_file, delimiter=',')
    for og_plus_pair, media in media_map.items():
        media_writer.writerow([
            json.dumps(og_plus_pair),
            json.dumps(media)
        ])
