import csv
import os
# with open("emails-20191024230221-XXXX.txt", newline='') as medias_file:
#     media_reader = csv.reader(medias_file, delimiter='\t')
#     for row in media_reader:
#         print(row[26])


for entry in os.scandir('bad_eds_email_fixes'):
    for filename in os.scandir(entry.path):
        with open(os.fsdecode(filename), newline='') as email_file:
            print(entry.path)
            email_reader = csv.reader(email_file, delimiter='\t')
            first_line = True
            for row in email_reader:
                if first_line:
                    print(row[0])
                    assert row[0] == "message_id"
                    assert row[26] == "bt_interesting_cause"
                    first_line = False
                assert len(row) == 27